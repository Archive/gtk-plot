// -*- C++ -*-

/* 
 * arraylayout.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "arraylayout.h"

#include "grid.h"

#include <iostream>
#include <vector>

/////////////////////////////////////////////////////

class ArrayLayoutImpl {
public:
  ArrayLayoutImpl() : x1_(0.0), y1_(0.0), x2_(0.0), y2_(0.0) {}
  ~ArrayLayoutImpl() {}

  // blow everything away.
  void clear(); 

  // These calls destroy any existing plot or axis
  void place_plot(PlotLayoutHandle* plh, size_t row, size_t col);
  void place_axis(AxisLayoutHandle* alh, size_t row, size_t col, Axis_Base::alignment_t a);

  // Destroy everything on a row or column and shift
  //  other rows/columns accordingly.
  void remove_row(size_t row);
  void remove_column(size_t col);
  
  // These don't shift other things, they just leave a blank.
  void erase_axis(size_t row, size_t col, Axis_Base::alignment_t a);
  void erase_plot(size_t row, size_t col);
  void erase_axes(size_t row, size_t col);
  // delete both axes and plots
  void erase_group(size_t row, size_t col);  
  
  void move_axis (size_t row1, size_t col1, size_t row2, size_t col2, 
		  Axis_Base::alignment_t a);
  void move_plot (size_t row1, size_t col1, size_t row2, size_t col2);
  void move_group(size_t row1, size_t col1, size_t row2, size_t col2);
  void swap_group(size_t row1, size_t col1, size_t row2, size_t col2);

  // Check on state
  bool plot_exists(size_t row, size_t col);
  bool axis_exists(size_t row, size_t col, Axis_Base::alignment_t a);
  bool group_exists(size_t row, size_t col);

  PlotLayoutHandle* get_plot(size_t row, size_t col);
  AxisLayoutHandle* get_axis(size_t row, size_t col, Axis_Base::alignment_t a);

  size_t rows() const; 
  size_t columns() const; 

  void shrink(size_t newrows, size_t newcols);

  void set_size(double x1, double y1, double x2, double y2);

  void do_layout();

protected:

private:
  // It's important that Grid initializes
  //  each element to 0.

  // plots_ is the canonical source of array size information.
  Grid<PlotLayoutHandle*> plots_;
  Grid<AxisLayoutHandle*> easts_;
  Grid<AxisLayoutHandle*> wests_;
  Grid<AxisLayoutHandle*> norths_;
  Grid<AxisLayoutHandle*> souths_;

  // Our bounding box
  double x1_, y1_, x2_, y2_;

  Grid<AxisLayoutHandle*>* axis_grid(Axis_Base::alignment_t a);

};

ArrayLayout::ArrayLayout()
  : impl_(new ArrayLayoutImpl)
{
  
}

ArrayLayout::~ArrayLayout()
{
  delete impl_;
}

 
void 
ArrayLayoutImpl::clear()
{
  plots_.clear();
  easts_.clear();
  wests_.clear();
  norths_.clear();
  souths_.clear();
} 

void 
ArrayLayoutImpl::place_plot(PlotLayoutHandle* plh, size_t row, size_t col)
{
  plots_.reserve(row+1,col+1);
  
  norths_.reserve(row+1,col+1);
  souths_.reserve(row+1,col+1);
  easts_.reserve(row+1,col+1);
  wests_.reserve(row+1,col+1);

  plots_[row][col] = plh;
}

void 
ArrayLayoutImpl::place_axis(AxisLayoutHandle* alh, size_t row, size_t col, 
			    Axis_Base::alignment_t a)
{
  g_return_if_fail(plots_.rows() > row);
  g_return_if_fail(plots_.columns() > col);

  g_return_if_fail(axis_exists(row, col, a));

  Grid<AxisLayoutHandle*>* grid;

  grid = axis_grid(a);

  (*grid)[row][col] = alh;
}

void 
ArrayLayoutImpl::remove_row(size_t row)
{
  g_return_if_fail(plots_.rows() > row);

  plots_.delete_row(row);
  norths_.delete_row(row);
  souths_.delete_row(row);
  easts_.delete_row(row);
  wests_.delete_row(row);
}

void 
ArrayLayoutImpl::remove_column(size_t col)
{
  g_return_if_fail(plots_.columns() > col);

  plots_.delete_column(col);
  norths_.delete_column(col);
  souths_.delete_column(col);
  easts_.delete_column(col);
  wests_.delete_column(col);
}

void 
ArrayLayoutImpl::erase_axis(size_t row, size_t col, Axis_Base::alignment_t a)
{
  g_return_if_fail(plots_.rows() > row);
  g_return_if_fail(plots_.columns() > col);

  Grid<AxisLayoutHandle*>* grid;
  
  grid = axis_grid(a);

  g_return_if_fail(grid != 0);
  g_return_if_fail(grid->rows() > row);
  g_return_if_fail(grid->columns() > col);
  
  (*grid)[row][col] = 0;
}

void 
ArrayLayoutImpl::erase_plot(size_t row, size_t col)
{
  g_return_if_fail(plots_.rows() > row);
  g_return_if_fail(plots_.columns() > col);

  plots_[row][col] = 0;
}

void ArrayLayoutImpl::erase_axes(size_t row, size_t col)
{
  g_return_if_fail(plots_.rows() > row);
  g_return_if_fail(plots_.columns() > col);

  norths_[row][col] = 0;
  souths_[row][col] = 0;
  easts_ [row][col] = 0;
  wests_ [row][col] = 0;
}

void 
ArrayLayoutImpl::erase_group(size_t row, size_t col)
{
  erase_plot(row,col);
  erase_axes(row,col);
}  

void 
ArrayLayoutImpl::move_axis (size_t row1, size_t col1, size_t row2, size_t col2, 
			    Axis_Base::alignment_t a)
{
  g_return_if_fail(plots_.rows() > row1);
  g_return_if_fail(plots_.columns() > col1);
  g_return_if_fail(plots_.rows() > row2);
  g_return_if_fail(plots_.columns() > col2);

  Grid<AxisLayoutHandle*>* grid;

  grid = axis_grid(a);
  
  (*grid)[row2][col2] = (*grid)[row1][col1];
  (*grid)[row1][col1] = 0;
}

void 
ArrayLayoutImpl::move_plot (size_t row1, size_t col1, size_t row2, size_t col2)
{
  g_return_if_fail(plots_.rows() > row1);
  g_return_if_fail(plots_.columns() > col1);
  g_return_if_fail(plots_.rows() > row2);
  g_return_if_fail(plots_.columns() > col2);

  plots_[row2][col2] = plots_[row1][col1];
  plots_[row1][col1] = 0;
}

void 
ArrayLayoutImpl::move_group(size_t row1, size_t col1, size_t row2, size_t col2)
{
  move_plot(row1,col1,row2,col2);
  move_axis(row1,col1,row2,col2,Axis_Base::NORTH);
  move_axis(row1,col1,row2,col2,Axis_Base::SOUTH);
  move_axis(row1,col1,row2,col2,Axis_Base::EAST);
  move_axis(row1,col1,row2,col2,Axis_Base::WEST);
}

void 
ArrayLayoutImpl::swap_group(size_t row1, size_t col1, size_t row2, size_t col2)
{
  g_return_if_fail(plots_.rows() > row1);
  g_return_if_fail(plots_.columns() > col1);
  g_return_if_fail(plots_.rows() > row2);
  g_return_if_fail(plots_.columns() > col2);

  PlotLayoutHandle* tmpplot;
  AxisLayoutHandle* tmpaxis;

  tmpplot = plots_[row2][col2];
  plots_[row2][col2] = plots_[row1][col1];
  plots_[row1][col1] = tmpplot;

  tmpaxis = norths_[row2][col2];
  norths_[row2][col2] = norths_[row1][col1];
  norths_[row1][col1] = tmpaxis;

  tmpaxis = easts_[row2][col2];
  easts_[row2][col2] = easts_[row1][col1];
  easts_[row1][col1] = tmpaxis;

  tmpaxis = wests_[row2][col2];
  wests_[row2][col2] = wests_[row1][col1];
  wests_[row1][col1] = tmpaxis;

  tmpaxis = souths_[row2][col2];
  souths_[row2][col2] = souths_[row1][col1];
  souths_[row1][col1] = tmpaxis;
}

bool 
ArrayLayoutImpl::plot_exists(size_t row, size_t col)
{
  g_return_val_if_fail(plots_.rows() > row, false);
  g_return_val_if_fail(plots_.columns() > col, false);

  return plots_[row][col] != 0;
}

bool 
ArrayLayoutImpl::axis_exists(size_t row, size_t col, Axis_Base::alignment_t a)
{
  g_return_val_if_fail(plots_.rows() > row, false);
  g_return_val_if_fail(plots_.columns() > col, false);
  
  Grid<AxisLayoutHandle*>* grid;

  grid = axis_grid(a);

  g_return_val_if_fail(grid != 0, false);
  g_return_val_if_fail(grid->rows() > row, false);
  g_return_val_if_fail(grid->columns() > col, false);  
  
  return (*grid)[row][col] != 0;
}


bool 
ArrayLayoutImpl::group_exists(size_t row, size_t col)
{
  return plot_exists(row,col)             || 
    axis_exists(row,col,Axis_Base::WEST)  ||
    axis_exists(row,col,Axis_Base::SOUTH) || 
    axis_exists(row,col,Axis_Base::EAST)  ||
    axis_exists(row,col,Axis_Base::WEST);
}

PlotLayoutHandle* 
ArrayLayoutImpl::get_plot(size_t row, size_t col)
{
  g_return_val_if_fail(plots_.rows() > row, 0);
  g_return_val_if_fail(plots_.columns() > col, 0);
  
  return plots_[row][col];
}

AxisLayoutHandle* 
ArrayLayoutImpl::get_axis(size_t row, size_t col, Axis_Base::alignment_t a)
{
  g_return_val_if_fail(plots_.rows() > row, 0);
  g_return_val_if_fail(plots_.columns() > col, 0);
  
  Grid<AxisLayoutHandle*>* grid;

  grid = axis_grid(a);

  g_return_val_if_fail(grid != 0, 0);
  g_return_val_if_fail(grid->rows() > row, 0);
  g_return_val_if_fail(grid->columns() > col, 0);  

  return (*grid)[row][col];
}

size_t 
ArrayLayoutImpl::rows() const 
{
  return plots_.rows();
} 

size_t 
ArrayLayoutImpl::columns() const 
{
  return plots_.columns();
} 

void 
ArrayLayoutImpl::shrink(size_t newrows, size_t newcols)
{
  plots_.shrink(newrows,newcols);
  norths_.shrink(newrows,newcols);
  souths_.shrink(newrows,newcols);
  easts_.shrink(newrows,newcols);
  wests_.shrink(newrows,newcols);
}

void 
ArrayLayoutImpl::set_size(double x1, double y1, double x2, double y2)  
{
  x1_ = x1;
  y1_ = y1;
  x2_ = x2;
  y2_ = y2;
}

void 
ArrayLayoutImpl::do_layout() 
{
  // Origin is at the bottom, this is a plot not X
  const double x_origin = x1_;
  const double y_origin = y2_;

  const double height = y2_ - y1_;
  const double width  = x2_ - x1_;

  const size_t rows = ArrayLayoutImpl::rows();
  const size_t columns = ArrayLayoutImpl::columns();

  // fixme, don't hardcode this.
  const double axis_inset = 10;
  
  // axis dimensions
  vector<double> east_axis_widths(columns);
  vector<double> west_axis_widths(columns);
  vector<double> north_axis_heights(rows);
  vector<double> south_axis_heights(rows);
  double total_vaxis_width = 0;
  double total_haxis_height = 0;

  double axis_size;
  size_t h = 0;
  while ( h < rows ) {
    north_axis_heights[h] = 0;
    south_axis_heights[h] = 0;

    size_t w = 0;
    while ( w < columns ) {
      AxisLayoutHandle* a = get_axis(h,w,Axis_Base::NORTH);
      if (a == 0) axis_size = 0; 
      else {
	a->get_size_hints(0,0,&axis_size,0);
      }

      north_axis_heights[h] = MAX(north_axis_heights[h], axis_size);
	
      a = get_axis(h,w,Axis_Base::SOUTH);
      if (a == 0) axis_size = 0; 
      else {
	a->get_size_hints(0,0,&axis_size,0);
      }

      south_axis_heights[h] = MAX(south_axis_heights[h], axis_size);
      ++w;
    }

    total_haxis_height += north_axis_heights[h];
    total_haxis_height += south_axis_heights[h];
    ++h;
  }

  size_t w = 0;
  while ( w < columns ) {

    east_axis_widths[w] = 0;
    west_axis_widths[w] = 0;

    size_t h = 0;
    while ( h < rows ) {
      AxisLayoutHandle* a = get_axis(h,w,Axis_Base::EAST);
      if (a == 0) axis_size = 0; 
      else {
	a->get_size_hints(0,0,&axis_size,0);
      }

      east_axis_widths[w] = MAX(east_axis_widths[w], axis_size);
	
      a = get_axis(h,w,Axis_Base::WEST);
      if (a == 0) axis_size = 0; 
      else {
	a->get_size_hints(0,0,&axis_size,0);
      }

      west_axis_widths[w] = MAX(west_axis_widths[w], axis_size);
      ++h;
    }

    total_vaxis_width += west_axis_widths[w];
    total_vaxis_width += east_axis_widths[w];
    ++w;
  }

  double total_hpadding = columns*axis_inset*2;

  double total_plot_width;

  if ((total_hpadding + total_vaxis_width) > width) {
    total_plot_width = 0;
  }
  else {
    total_plot_width = width - total_vaxis_width - total_hpadding;
  }

  double total_vpadding = rows*axis_inset*2;
  
  double total_plot_height;
  if ((total_vpadding + total_haxis_height) > height) {
    total_plot_height = 0;
  }
  else {
    total_plot_height = height - total_haxis_height - total_vpadding;
  }

  const double per_plot_width   = total_plot_width / columns;
  const double per_plot_height  = total_plot_height / rows;

  const double per_haxis_width  = per_plot_width + axis_inset*2;
  const double per_vaxis_height = per_plot_height + axis_inset*2;

    
  // We are finally ready to do the allocations.
  // Row 0 starts at the bottom left, at y_origin and x_origin.
  // x_next counts up and y_next counts down.


  ///////////////// FIXME handle not-enough-space

  double x_next = x_origin;

  w = 0;             // index for widths (column number)
  while ( w < columns ) {
    double y_next = y_origin;

    h = 0;     // h is the index for heights (row number)

    while ( h < rows ) {      
      PlotLayoutHandle* plot;
      AxisLayoutHandle* axis;
      
      plot = get_plot(h,w);
	
      if (plot != 0) {
	plot->set_size(x_next + west_axis_widths[w] + axis_inset,
		       y_next - south_axis_heights[h] - axis_inset - per_plot_height,
		       per_plot_width,
		       per_plot_height);
      }
	
      // SOUTH
      axis = get_axis(h,w,Axis_Base::SOUTH );
      
      if (axis != 0) {
	axis->set_size(x_next + west_axis_widths[w],
		       y_next - south_axis_heights[h],
		       per_haxis_width,
		       south_axis_heights[h]);
      }
			 
      // WEST
      axis = get_axis(h,w,Axis_Base::WEST );
      
      if (axis != 0) {
	axis->set_size(x_next,
		       y_next - south_axis_heights[h] - per_vaxis_height,
		       west_axis_widths[w],
		       per_vaxis_height);

      }

      // NORTH
      axis = get_axis(h,w,Axis_Base::NORTH );
	
      if (axis != 0) {
	axis->set_size(x_next + west_axis_widths[w],
		       y_next - south_axis_heights[h] - 
		       per_vaxis_height - north_axis_heights[h],
		       per_haxis_width,
		       north_axis_heights[h]);
      }
	
      // EAST
      axis = get_axis(h,w,Axis_Base::EAST );
	
      if (axis != 0) {
	axis->set_size( x_next + per_haxis_width + west_axis_widths[w],
			y_next - south_axis_heights[h] - per_vaxis_height,
			east_axis_widths[w],
			per_vaxis_height);	
      }

      y_next -= (south_axis_heights[h] + per_vaxis_height + north_axis_heights[h]);

      ++h;
    }

    x_next += (east_axis_widths[w] + per_haxis_width + west_axis_widths[w]); 
    ++w;
  }
}

Grid<AxisLayoutHandle*>*
ArrayLayoutImpl::axis_grid(Axis_Base::alignment_t a)
{
  switch (a) {
  case Axis_Base::NORTH:
    return &norths_;
    break;
  case Axis_Base::SOUTH:
    return &souths_;
    break;
  case Axis_Base::EAST:
    return &easts_;
    break;
  case Axis_Base::WEST:
    return &wests_;
    break;
  default:
    g_warning("Bad alignment %d", a);
    return 0;
    break;
  }
}


///////////////////////////////////////////////////////////////////////
/// Stubs to go from ArrayLayout to ArrayLayoutImpl

void ArrayLayout::clear() {
  impl_->clear(); 
} 

void ArrayLayout::place_plot(PlotLayoutHandle* plh, size_t row, size_t col) {
  impl_->place_plot(plh,row,col);
}
void ArrayLayout::place_axis(AxisLayoutHandle* alh, size_t row, size_t col, Axis_Base::alignment_t a) {
  impl_->place_axis(alh,row,col,a);
}

void ArrayLayout::remove_row(size_t row) {
  impl_->remove_row(row);
}
void ArrayLayout::remove_column(size_t col) {
  impl_->remove_column(col);
}

void ArrayLayout::erase_axis(size_t row, size_t col, Axis_Base::alignment_t a) {
  impl_->erase_axis(row,col,a);
}
void ArrayLayout::erase_plot(size_t row, size_t col) {
  impl_->erase_plot(row,col);
}
void ArrayLayout::erase_axes(size_t row, size_t col) {
  impl_->erase_axes(row,col);
}

void ArrayLayout::erase_group(size_t row, size_t col) {
  impl_->erase_group(row,col);
}  

void ArrayLayout::move_axis (size_t row1, size_t col1, size_t row2, size_t col2, 
				   Axis_Base::alignment_t a)
{
  impl_->move_axis(row1,col1,row2,col2,a);
}

void ArrayLayout::move_plot (size_t row1, size_t col1, size_t row2, size_t col2)
{
  impl_->move_plot(row1,col1,row2,col2);
}

void ArrayLayout::move_group(size_t row1, size_t col1, size_t row2, size_t col2) {
  impl_->move_group(row1,col1,row2,col2);
}
void ArrayLayout::swap_group(size_t row1, size_t col1, size_t row2, size_t col2) {
  impl_->swap_group(row1,col1,row2,col2);
}

bool ArrayLayout::plot_exists(size_t row, size_t col) {
  return impl_->plot_exists(row,col);
}
bool ArrayLayout::axis_exists(size_t row, size_t col, Axis_Base::alignment_t a) {
  return impl_->axis_exists(row,col,a);
}
bool ArrayLayout::group_exists(size_t row, size_t col) {
  return impl_->group_exists(row,col);
}

PlotLayoutHandle* ArrayLayout::get_plot(size_t row, size_t col) {
  return impl_->get_plot(row,col);
}

AxisLayoutHandle* ArrayLayout::get_axis(size_t row, size_t col, Axis_Base::alignment_t a) {
  return impl_->get_axis(row,col,a);
}

size_t ArrayLayout::rows() const {
  return impl_->rows();
} 

size_t ArrayLayout::columns() const {
  return impl_->columns();
} 

void ArrayLayout::shrink(size_t newrows, size_t newcols) {
  impl_->shrink(newrows,newcols);
}

void ArrayLayout::set_size(double x1, double y1, double x2, double y2)  {
  impl_->set_size(x1, y1, x2, y2);
}

void ArrayLayout::do_layout() {
  impl_->do_layout();
}






