// -*- C++ -*-

/* 
 * arraylayout.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef GUPPI_ARRAYLAYOUT_H
#define GUPPI_ARRAYLAYOUT_H

#include "axis_base.h"

#include <stdlib.h> // size_t

class ArrayLayoutImpl;

// These routines handle the layout of an array of plots groups.  A
// "group" is one plot and 4 axes, one of each alignment.

// Subclass the handles to contain widgets or canvas items or whatever
// you want to use; then stick them in your ArrayLayout object.

class LayoutHandle {
public:
  // the ArrayLayout will use this to ask for any size hints from the item
  //  being layed out.  If any pointer is NULL, it should be ignored.
  virtual void get_size_hints(double* x, double* y, double* w, double* h) = 0;
  // Assign a size, which the item *must* accept. Can be fractional.
  virtual void set_size      (double x, double y, double w, double h) = 0;

};

class AxisLayoutHandle : public LayoutHandle {
public:
  // Assign the portion of the size that matches the plot. i.e. the
  // axis is assigned more width or height than the plot, in order to
  // display labels or whatever; this part of the axis object should
  // actually be spanned by the drawn axis.
  virtual void set_bounds(double start, double stop) = 0;
}; 

class PlotLayoutHandle : public LayoutHandle {
public:

  // just exists for future expansion

};

class ArrayLayout {
public:
  ArrayLayout();
  ~ArrayLayout();

  // blow everything away.
  void clear(); 

  // Add this plot or axis.
  void place_plot(PlotLayoutHandle* plh, size_t row, size_t col);
  void place_axis(AxisLayoutHandle* alh, size_t row, size_t col, Axis_Base::alignment_t a);

  // Destroy everything on a row or column and shift
  //  other rows/columns accordingly.
  void remove_row(size_t row);
  void remove_column(size_t col);
  
  // These don't shift other things, they just leave a blank.
  void erase_axis(size_t row, size_t col, Axis_Base::alignment_t a);
  void erase_plot(size_t row, size_t col);

  // kill all 4 axes at once.
  void erase_axes(size_t row, size_t col);

  // delete both axes and plots
  void erase_group(size_t row, size_t col);  
  
  // move stuff, overwriting if necessary
  void move_axis (size_t row1, size_t col1, size_t row2, size_t col2, 
		  Axis_Base::alignment_t a);
  void move_plot (size_t row1, size_t col1, size_t row2, size_t col2);
  void move_group(size_t row1, size_t col1, size_t row2, size_t col2);
  void swap_group(size_t row1, size_t col1, size_t row2, size_t col2);

  // Check on state
  bool plot_exists(size_t row, size_t col);
  bool axis_exists(size_t row, size_t col, Axis_Base::alignment_t a);
  bool group_exists(size_t row, size_t col);

  PlotLayoutHandle* get_plot(size_t row, size_t col);
  AxisLayoutHandle* get_axis(size_t row, size_t col, Axis_Base::alignment_t a);

  size_t rows() const; 
  size_t columns() const; 

  // place_* automatically grows as needed, but you need this to reduce size
  void shrink(size_t newrows, size_t newcols);
  
  // Set the area to lay out our stuff in; origin at top left, 
  //   like X, but the resulting layout is with the origin at 
  //   bottom left.
  void set_size(double x1, double y1, double x2, double y2);

  // Perform layout on all handles.
  void do_layout();

private:
  ArrayLayoutImpl* impl_;
};

#endif





