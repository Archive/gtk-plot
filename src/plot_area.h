// This is -*- C++ -*-
// $Id$

/*
 * plot_area.h
 *
 * Copyright (C) 1998 EMC Capital Management Inc. 
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef _INC_PLOT_AREA_H
#define _INC_PLOT_AREA_H

#include "plot_draw.h"
#include "axis_base.h"
#include "plot_transform.h"
#include "plot_data.h"
#include "bounded.h"

class Plot_Area : public Bounded {
public:
  
  Plot_Area() : 
    target_(0),
    lineplot_buffer_(0), lineplot_buffer_size_(0)
    {}

  virtual ~Plot_Area();

  void set_drawing_target(Plot_Draw* d);
  Plot_Draw* drawing_target() { return target_; }

  virtual void set_x_bounds(gdouble x0, gdouble x1);
  virtual void set_y_bounds(gdouble y0, gdouble y1);

  void set_bounds(gdouble x0, gdouble x1, gdouble y0, gdouble y1) {
    set_x_bounds(x0, x1);
    set_y_bounds(y0, y1);
  }

  void set_x_bounds(const Axis_Base* a) { set_x_bounds(a->min(), a->max()); }
  void set_y_bounds(const Axis_Base* a) { set_y_bounds(a->min(), a->max()); }
  void set_bounds(const Axis_Base* ax, const Axis_Base* ay) {
    set_x_bounds(ax);
    set_y_bounds(ay);
  }

  const Plot_Transform* x_transform() const { return &x_trans_; }
  const Plot_Transform* y_transform() const { return &y_trans_; }

  ///////////////////////////////////////////////////////////////////////////

  /// Drawing primitives

  enum marker_t {
    NONE,           // Nothing - makes the plot invisible
    DOT,            // Single pixel point
    CIRCLE,         // Unfilled circle
    FILLED_CIRCLE,  //etc.
    BOX, 
    FILLED_BOX,
    DIAMOND, 
    FILLED_DIAMOND, 
    PLUS,            // +
    TIMES,           // x
    HORIZONTAL_TICK, //  -
    VERTICAL_TICK,   // |
    HORIZONTAL_RULE, // These aren't markers exactly, but can be drawn the same
    VERTICAL_RULE,   // way
    PLUS_RULE
  };

  void draw_marker(marker_t shape, gdouble x, gdouble y, gint size = -1);

  /*

     Gratuitous ASCII art:

                 -------- length
               |  /|
     head      | / +----+  | thickness
     thickness | \ +----+  | 
               |  \|
                 --- head length

     (x,y) = point at the tip of the head of the arrow
     angle specifies how to rotate the arrow, in radians
   */
  void draw_arrow(gint x, gint y,
		  double angle,
		  double length, double thickness,
		  double head_length, double head_thickness,
		  bool filled);

  void draw_horizontal_rule(double y);

  void draw_vertical_rule(double x);

  void draw_plus_rule(double x, double y) {
    draw_horizontal_rule(y);
    draw_vertical_rule(x);
  }

  // Useful for plotting regression lines and such.
  // This will, in particular, really do the wrong thing if clipping
  // isn't set up properly!
  void draw_line_through_points(double x0, double y0,
				double x1, double y1);

  void draw_line(double x0, double y0, double x1, double y1);

  ///////////////////////////////////////////////////////////////////////////

  /*

    draw_line_plot() is optimized to allow for very fast plotting of
    line graphs.

    Each time draw_line_plot() is called, the line-drawing information
    calculated by the function is saved.  The same set of joined lines
    is redrawn when redraw_line_plot() is called.

    Calling reset_line_plot() causes any information that has been
    saved by a previous call to draw_line_plot() to be "forgotten".

    This can be useful for doing things like using XOR drawing to animate
    a moving line graph.

  */

  void draw_line_plot(const Plot_Data& x,
                      const Plot_Data& y);

  void redraw_line_plot();

  void reset_line_plot() {
    delete [] lineplot_buffer_;
    lineplot_buffer_ = 0;
    lineplot_buffer_size_ = 0;
  }

  void draw_scatter_plot(const Plot_Data & x_data,
			 const Plot_Data & y_data,
			 marker_t marker = DOT,
			 gint market_size = -1);

  void draw_price_bars(const Plot_Data& t,
		       const Plot_Data& open,
		       const Plot_Data& high,
		       const Plot_Data& low,
		       const Plot_Data& close,
		       double min_spacing = 1.0);

  // Many more exotic plot types can/will be added later...

private: 

  Plot_Transform x_trans_, y_trans_;

  Plot_Draw* target_;

  GdkPoint* lineplot_buffer_;
  size_t lineplot_buffer_size_;  
};

#endif // _INC_PLOT_AREA_H

// $Id$

