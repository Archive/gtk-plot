// -*- C++ -*-

/* 
 * plot_area.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include "plot_area.h"

#define G_ENABLE_DEBUG


Plot_Area::~Plot_Area()
{
  delete [] lineplot_buffer_;
}

void
Plot_Area::set_drawing_target(Plot_Draw* d)
{
  target_ = d;

  // We set things up so that the origin is in the lower left corner,
  // since this is usually the way we want to do things.
  x_trans_.set_screen_bounds(target_->x_origin(), 
			     target_->x_origin() + target_->width());
  y_trans_.set_screen_bounds(target_->y_origin() + target_->height(), 
			     target_->y_origin());
}

void
Plot_Area::set_x_bounds(gdouble x0, gdouble x1)
{
  Bounded::set_x_bounds(x0,x1);
  x_trans_.set_range_bounds(x_min(),x_max());
}

void
Plot_Area::set_y_bounds(gdouble y0, gdouble y1)
{
  Bounded::set_y_bounds(y0,y1);
  y_trans_.set_range_bounds(y_min(),y_max());
}

void 
Plot_Area::draw_marker(marker_t shape,
		       gdouble x, gdouble y, gint size)
{
  if (shape == NONE) return;
  if (size < 0) size = 4; // DEFAULT

  gint tx = x_trans_.transform(x);
  gint ty = y_trans_.transform(y);

  switch(shape) {
  case DOT:
    target_->draw_point(tx, ty);
    break;

  case CIRCLE:  // Fall through
  case FILLED_CIRCLE:
    target_->draw_circle((shape == FILLED_CIRCLE), 
			 tx, ty, size);
    break;

  case BOX:    // Fall through
  case FILLED_BOX:
    target_->draw_rectangle_coor((shape == FILLED_BOX), 
				 tx - size, ty - size, 
				 tx + size, ty + size);
    break;

  case DIAMOND: // Falls through
  case FILLED_DIAMOND: {
    gint adj_size = (gint)(size*0.7071+0.5); // 0.7071 ~ sqrt(2)/2
    GdkPoint pts[5];
    pts[0].x = pts[4].x = tx; 
    pts[0].y = pts[4].y = ty + adj_size;
    pts[1].x = tx - adj_size; 
    pts[1].y = ty;
    pts[2].x = tx; 
    pts[2].y = ty - adj_size;
    pts[3].x = tx + adj_size; 
    pts[3].y = ty;
    target_->draw_polygon((shape == FILLED_DIAMOND), pts, 5);
    break;
  }

  case PLUS:
    // probably optimizable
    target_->draw_line(tx, ty + size, tx, ty - size);
    target_->draw_line(tx + size, ty, tx - size, ty);
    break;

  case TIMES: {
    gint adj_size = (gint)(size*0.7071+0.5);
    target_->draw_line(tx + adj_size, ty + adj_size, 
		       tx - adj_size, ty - adj_size);
    target_->draw_line(tx + adj_size, ty - adj_size, 
		       tx - adj_size, ty + adj_size);
    break;
  }

  case HORIZONTAL_TICK:
    target_->draw_line(tx + size, ty, tx - size, ty);
    break;

  case VERTICAL_TICK:
    target_->draw_line(tx, ty + size, tx, ty - size);
    break;

  case HORIZONTAL_RULE:
    draw_horizontal_rule(y);
    break;

  case VERTICAL_RULE:
    draw_vertical_rule(x);
    break;

  case PLUS_RULE:
    draw_plus_rule(x, y);
    break;

  default:
    g_warning("Unknown marker type");
    break;
  }
}

inline void arrow_rot_trans(GdkPoint& p, double s, double c,
			    gint x0, gint y0, double x, double y)
{
  p.x = x0 + (gint)(x*c+y*s);
  p.y = y0 + (gint)(y*c-x*s);
}

void
Plot_Area::draw_arrow(gint x0, gint y0,
		      double angle, double l, double t, double hl, double ht,
		      bool filled)
{
  GdkPoint pts[8];
  double s = sin(angle);
  double c = cos(angle);

  // Providing the last point == to the first pont makes the draw_polygon
  // call slightly more efficient.
  pts[0].x = pts[7].x = x0;
  pts[0].y = pts[7].y = y0;

  arrow_rot_trans(pts[1], s, c, x0, y0, ht/2, hl);
  arrow_rot_trans(pts[2], s, c, x0, y0, t/2, hl);
  arrow_rot_trans(pts[3], s, c, x0, y0, t/2, l);
  arrow_rot_trans(pts[4], s, c, x0, y0, -t/2, l);
  arrow_rot_trans(pts[5], s, c, x0, y0, -t/2, hl);
  arrow_rot_trans(pts[6], s, c, x0, y0, -ht/2, hl);

  target_->draw_polygon(filled, pts, 8);
}

void 
Plot_Area::draw_horizontal_rule(double y)
{
  gint yy = y_trans_.transform(y);
  target_->draw_line(target_->x_origin(), yy, target_->x_origin() + target_->width(), yy);
}

void 
Plot_Area::draw_vertical_rule(double x)
{
  gint xx = x_trans_.transform(x);
  target_->draw_line(xx, target_->y_origin(), xx, target_->y_origin()+target_->height());
}

void
Plot_Area::draw_line_through_points(double x0, double y0,
				    double x1, double y1)
{
  // Check if (x0,y0) == (x1,y1) in a way that 
  // (a) is "floating point safe"
  // (b) Allows for the fact that you might have set things up
  //     so that |x0-x1| and |y0-y1| are very small, and want it that way.
  // No doubt we are being too clever for our own good.
  g_return_if_fail(  (fabs(x0-x1) > 1e-8*(fabs(x0)+fabs(x1))) ||
		     (fabs(y0-y1) > 1e-8*(fabs(y0)+fabs(y1))) );
  g_return_if_fail(target_ != 0);

  // line has eqn ax+by=c
  double a = y0-y1;
  double b = x1-x0;
  double c = a*x0+b*y0; // could just as well be a*x1+b*y1...

  gint tx0, ty0, tx1, ty1;

  // We go through this trouble to try to avoid dividing by zero.
  // This code will handle horizontal & vertical lines just fine,
  // by using the version of the calculation that allows it to do the
  // divisions by max(|a|,|b|).


  ///////////////////////// FIXME this is wrong, because 
    /////////////////////// the inverse of x_origin() != x_min()
    // (maybe copy code from lineactor in guppi)

  if (fabs(a) > fabs(b)) {
    ty0 = target_->y_origin();
    ty1 = target_->y_origin()+target_->height();
    tx0 = x_trans_.transform((c-b*y_max())/a);
    tx1 = x_trans_.transform((c-b*y_min())/a);
  } else {
    tx0 = target_->x_origin();
    tx1 = target_->x_origin()+target_->width();
    ty0 = y_trans_.transform((c-a*x_min())/b);
    ty1 = y_trans_.transform((c-a*x_max())/b);
  }

  g_print("Drawing line from %d, %d to %d, %d\n",
	  tx0, ty0, tx1, ty1);

  target_->draw_line(tx0, ty0, tx1, ty1);
}

void 
Plot_Area::draw_line(double x0, double y0, double x1, double y1)
{
  g_return_if_fail(target_ != 0);
  gint tx0,ty0,tx1,ty1;

  tx0 = x_trans_.transform(x0);
  ty0 = y_trans_.transform(y0);
  tx1 = x_trans_.transform(x1);
  ty1 = y_trans_.transform(y1);

  target_->draw_line(tx0,ty0,tx1,ty1);
}

///////////////////////////////////////////////////////////////////////////


void
Plot_Area::draw_line_plot(const Plot_Data& x, const Plot_Data& y)
{
  g_return_if_fail(x.size() == y.size());

  if (lineplot_buffer_ == 0 || x.size() != lineplot_buffer_size_) {
    delete [] lineplot_buffer_;
    lineplot_buffer_size_ = x.size();
    lineplot_buffer_ = new GdkPoint[lineplot_buffer_size_];
  }

  Plot_Data::const_iterator ix = x.begin(), iy = y.begin();
  int j=0;
  while (ix != x.end()) {
    lineplot_buffer_[j].x = x_trans_.transform(*ix);
    lineplot_buffer_[j].y = y_trans_.transform(*iy);
    ++j;
    ++ix;
    ++iy;
  }

  target_->draw_lines(lineplot_buffer_, lineplot_buffer_size_);
}

void
Plot_Area::redraw_line_plot()
{
  g_return_if_fail( lineplot_buffer_ && lineplot_buffer_size_ > 0 );

  target_->draw_lines(lineplot_buffer_, lineplot_buffer_size_);
}

void 
Plot_Area::draw_scatter_plot(const Plot_Data & x, 
			     const Plot_Data & y,
			     marker_t marker,
			     gint marker_size)
{
  g_return_if_fail(x.size() == y.size());

  Plot_Data::const_iterator ix0, ix1, iy0, iy1;

  const double x_slop = 0.15 * x_size();
  const double y_slop = 0.15 * y_size();

  // Can't just || these two expressions inside the if, because both need
  // to be called no matter what.  (They init i{x,y}{0,1}, even if the
  // data isn't sorted, and || can short-circuit the second function.)
  bool rv_x = x.find_bounding_iterators(x_min()-x_slop,x_max()+x_slop,ix0,ix1);
  bool rv_y = y.find_bounding_iterators(y_min()-y_slop,y_max()+y_slop,iy0,iy1);
  if (rv_x || rv_y)
    synchronize(ix0, ix1, iy0, iy1);

  if (ix0 == ix1) // There is nothing to draw
    return;

  if (marker == DOT) { 
    // We optimize the case of dot-drawing.  Despite the overhead of
    // allocing and freeing memory, I get a speed-up of about 30% on
    // very large scatter plots.  (And anyway, my benchmarks suggest
    // that the new/delete[] overhead is really quite minimal.)
    size_t N = ix1.underlying_position() - ix0.underlying_position();
    GdkPoint* pts = new GdkPoint[N];
    size_t i=0;
    while (ix0 != ix1) {
      pts[i].x = x_trans_.transform(*ix0);
      pts[i].y = y_trans_.transform(*iy0);
      ++ix0;
      ++iy0;
      ++i;
    }
    target_->draw_points(pts, N);
    delete [] pts;
  } else {
    // Since we are synchronized, a single check for
    // the end-of-data is enough.
    while (ix0 != ix1) {
      draw_marker(marker, *ix0, *iy0, marker_size);
      ++ix0;
      ++iy0;
    }
  }
}

void
Plot_Area::draw_price_bars(const Plot_Data& t,
			   const Plot_Data& op,
			   const Plot_Data& hi,
			   const Plot_Data& lo,
			   const Plot_Data& cl,
			   gdouble min_spacing)
{
  g_return_if_fail(t.size() == op.size() &&
		   t.size() == hi.size() &&
		   t.size() == lo.size() &&
		   t.size() == cl.size());

  const int N = 3*t.size();
  GdkSegment* seg = new GdkSegment[N];
  int j=0;

  gint tick_size=(x_trans_.transform(10*min_spacing)-x_trans_.transform(0))/25;
  if (tick_size < 1) tick_size = 1;

  // When price bars are spaced out a lot, I think that they are more
  // attractive when they have a bit of thickness to them.
  gint line_thickness = tick_size/2;
  if (line_thickness < 1) line_thickness = 1;

  Plot_Data::const_iterator it = t.begin(), io = op.begin(),
    ih = hi.begin(), il = lo.begin(), ic = cl.begin();

  while (it != t.end()) {
    gint tt = x_trans_.transform(*it);

    // main vertical bar
    seg[j].x1 = seg[j].x2 = tt;
    seg[j].y1 = y_trans_.transform(*ih);
    seg[j].y2 = y_trans_.transform(*il);

    ++j;

    // open tick to left of main bar
    seg[j].y1 = seg[j].y2 = y_trans_.transform(*io);
    seg[j].x1 = tt-tick_size;
    seg[j].x2 = tt;

    ++j;
    
    // close tick to right of main bar
    seg[j].y1 = seg[j].y2 = y_trans_.transform(*ic);
    seg[j].x1 = tt;
    seg[j].x2 = tt+tick_size;

    ++j;
    
    ++it, ++io, ++ih, ++il, ++ic;
  }

  target_->gc_clone();
  target_->gc()->set_line_width(line_thickness);
  target_->draw_segments(seg, N);
  target_->gc_restore();

  delete [] seg;
}

