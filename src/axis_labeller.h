// This is -*- C++ -*-
// $Id$

/* 
 * axis_labeller.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef _INC_AXIS_LABELLER_H
#define _INC_AXIS_LABELLER_H

#include "axis_base.h"

class Axis_Labeller {
public:
  Axis_Labeller(Axis_Base* a) : primary_(a), mirror_(0),
    label_mirror_(false), locked_bounds_(true)
    { }

  virtual ~Axis_Labeller() { }

  void set_mirror(Axis_Base* m) { mirror_ = m; }
  bool set_mirror_labelling(bool f) { return label_mirror_ = f; }

  bool locked_bounds() const { return locked_bounds_; }
  void set_locked_bounds(bool x) { locked_bounds_ = x; }

  virtual void label() = 0;

  // All drawing functions are meant purely for internal use by subclasses,
  //  but there are some cases where they need to be public. 

  void draw_tick(gdouble pos, gint size) {
    primary_->draw_tick(pos, size);
    if (mirror_) mirror_->draw_tick(pos, size);
  }

  void draw_minor_tick(gdouble pos) {
    primary_->draw_minor_tick(pos);
    if (mirror_) mirror_->draw_minor_tick(pos);
  }

  void draw_major_tick(gdouble pos) {
    primary_->draw_major_tick(pos);
    if (mirror_) mirror_->draw_major_tick(pos);
  }

  void draw_label(gdouble pos, gint adjust, const char* mylabel,
                  bool superstyle = false, 
                  bool leftalign = false) {
    primary_->draw_label(pos,adjust,mylabel,superstyle,leftalign);
    if (mirror_ && label_mirror_)
      mirror_->draw_label(pos,adjust,mylabel,superstyle,leftalign);
  }

  void draw_numeric_label(gdouble pos, gint adjust,
			  const char* printf_style_format,
			  double value) {
    primary_->draw_numeric_label(pos,adjust,printf_style_format,value);
    if (mirror_ && label_mirror_)
      mirror_->draw_numeric_label(pos,adjust,printf_style_format,value);
  }

protected:
  void reset_bounds(double a, double b) {
    primary_->set_bounds(a,b);
    if (mirror_) mirror_->set_bounds(a,b);
  }


  Axis_Base* primary() const { return primary_; }
  Axis_Base* mirror() const { return mirror_; }

private:
  Axis_Base* primary_;
  Axis_Base* mirror_;

  bool label_mirror_;
  bool locked_bounds_;
};

#endif // _INC_AXIS_LABELLER_H

// $Id$
