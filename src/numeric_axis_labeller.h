// This is -*- C++ -*-
// $Id$

/* 
 * numeric_axis_labeller.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef _INC_NUMERIC_AXIS_LABELLER_H
#define _INC_NUMERIC_AXIS_LABELLER_H

#include "axis_labeller.h"

class Numeric_Axis_Labeller : public Axis_Labeller {
public:
  Numeric_Axis_Labeller(Axis_Base* a) : Axis_Labeller(a), goal_(5)
    { }

  void set_label_count_goal(gint g) { goal_ = g; }

  virtual void label();

private:
  gint goal_;
};

#endif // _INC_NUMERIC_AXIS_LABELLER_H

// $Id$
