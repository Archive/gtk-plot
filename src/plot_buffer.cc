// This is -*- C++ -*-
// $Id$

/* 
 * plot_buffer.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include "plot_buffer.h"

Plot_Buffer::Plot_Buffer() :
  da_(0), width_(0), height_(0), buffer_(0), idle_connected_(false)
{
  da_ = gtk_drawing_area_new();

  gtk_signal_connect(GTK_OBJECT(da_),
                     "expose_event",
                     GTK_SIGNAL_FUNC(expose_event_cb),
                     this);

  gtk_signal_connect(GTK_OBJECT(da_),
                     "configure_event",
                     GTK_SIGNAL_FUNC(configure_event_cb),
                     this);

  // Make sure we get all of the events that Gtk_DrawingArea doesn't
  // usually see.
  gint mask = gtk_widget_get_events (da_);
  mask |= GDK_EXPOSURE_MASK;
  mask |= GDK_POINTER_MOTION_MASK;
  //  mask |= GDK_POINTER_MOTION_HINT_MASK; // interferes with regular pointer motion events.
  mask |= GDK_BUTTON1_MOTION_MASK;
  mask |= GDK_BUTTON_PRESS_MASK;
  mask |= GDK_BUTTON_RELEASE_MASK;
  mask |= GDK_KEY_PRESS_MASK;
  mask |= GDK_ENTER_NOTIFY_MASK;
  mask |= GDK_LEAVE_NOTIFY_MASK;
  gtk_widget_set_events(da_, mask);
}

Plot_Buffer::~Plot_Buffer()
{
  disconnect_idle(true);

  if (buffer_)
    gdk_pixmap_unref(buffer_);
}

gint
Plot_Buffer::expose_event_impl(GdkEventExpose* e)
{
  refresh(e->area.x, e->area.y, e->area.width, e->area.height);
  return TRUE;
}

gint
Plot_Buffer::configure_event_impl(GdkEventConfigure* c)
{
  if ( (c->width != width_) || (c->height != height_) ) {
    new_pixmap(c);
    if (buffer_ != 0) redraw();
  }
  return TRUE;
}

void 
Plot_Buffer::new_pixmap(GdkEventConfigure * c)
{
  // Create a new pixmap if the size changes.
  if ( (c->width != width_) || (c->height != height_) ) {
    if (buffer_)
      gdk_pixmap_unref(buffer_);
    
    width_ = c->width;
    height_ = c->height;

    // Don't create a pixmap with a 0 dimension
    if (width_ > 0 && height_ > 0) {
      buffer_ = gdk_pixmap_new(da_->window,
			       width_,height_,
			       -1);
    }
    else buffer_ = 0;
  }
}

void
Plot_Buffer::refresh(gint x, gint y, gint w, gint h)
{
  if (buffer_ != 0) {
    gdk_draw_pixmap(da_->window,
		    da_->style->fg_gc[GTK_WIDGET_STATE (da_)],
		    buffer_,
		    x,y,x,y,w,h);
  }
}

void
Plot_Buffer::queue_redraw()
{
  if (!idle_connected_) {
    connection_id_ =  gtk_idle_add(redrawidle, this);
    idle_connected_ = true;
  }
}

gint
Plot_Buffer::redrawidle(gpointer data)
{
  Plot_Buffer* pb = static_cast<Plot_Buffer*>(data);
  g_return_val_if_fail(pb != 0, FALSE);

  pb->redraw();

  pb->disconnect_idle(false);

  return FALSE; // disconnect
}

void
Plot_Buffer::disconnect_idle(bool remove)
{
  if (idle_connected_) {
    if (remove) gtk_idle_remove(connection_id_);
    idle_connected_ = false;
  }
}

gint 
Plot_Buffer::expose_event_cb(GtkWidget* w, GdkEventExpose* e, gpointer data)
{
  Plot_Buffer* pb = static_cast<Plot_Buffer*>(data);
  g_return_val_if_fail(pb != 0, FALSE);

  return pb->expose_event_impl(e);
}

gint 
Plot_Buffer::configure_event_cb(GtkWidget* w, GdkEventConfigure* e, gpointer data)
{
  Plot_Buffer* pb = static_cast<Plot_Buffer*>(data);
  g_return_val_if_fail(pb != 0, FALSE);
  
  return pb->configure_event_impl(e);
}

// $Id$



