// -*- C++ -*-

/* 
 * axis_base.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

/* There's an awful lot of cut-and-paste between this and Plot_Area */

/* The my_gc needs to be pushed onto the target instead of used in each 
   drawing call, which requires push/pop implementation in the draw object. */

#ifndef _INC_AXIS_BASE_H
#define _INC_AXIS_BASE_H

#include "plot_draw.h"
#include "plot_transform.h"
#include "plot_data.h"

class Axis_Base {
public:

  enum alignment_t { NORTH, SOUTH, EAST, WEST };

  // FIXME add initializers for all the members that need it
  Axis_Base(alignment_t a) : align_(a), target_(0), min_(0), max_(1), inset_(0)
  { }

  virtual ~Axis_Base() {}

  alignment_t alignment() const { return align_; }
  bool horizontal() const { return alignment()==NORTH || alignment()==SOUTH; }
  bool vertical() const { return !horizontal(); }

  const Plot_Transform* transform() const { return &trans_; }

  virtual void set_drawing_target(Plot_Draw* d);
  Plot_Draw* drawing_target() { return target_; }

  // Set the inset from either end; need to have some inset to 
  //  get the labels inside the target drawable
  virtual void set_axis_line_inset(gint inset);


  virtual void set_bounds(gdouble min, gdouble max);
  void set_bounds(Axis_Base* a) { set_bounds(a->min(), a->max()); }

  gdouble min() const { return min_; }
  gdouble max() const { return max_; }
  gdouble size() const { return max_-min_; }
  bool in_bounds(gdouble t) const { return min_ <= t && t <= max_; }

  // Just draw a line along the appropriate side of the drawing area.
  virtual void draw_axis_line();
  virtual void draw_tick(double pos, gint size, bool heavy = false);
  virtual void draw_minor_tick(double pos) {
    draw_tick(pos,minor_tick_size());
  }
  virtual void draw_major_tick(double pos) {
    draw_tick(pos,major_tick_size(),true);
  }
  virtual void draw_label(double pos, gint adjust, 
			  const char* label, 
                          bool superstyle = false, 
                          bool leftalign = false);

  virtual void draw_numeric_label(double pos, gint adjust,
				  const char* printf_style_format, 
				  double value);

  static alignment_t opposite(alignment_t a) {
    switch (a) {
    case NORTH: return SOUTH;
    case SOUTH: return NORTH;
    case EAST: return WEST;
    case WEST: return EAST;
    default: return SOUTH; // -Wall
    };
  }

  // Return width/height of the axis, counting the inset.
  gint axis_width() const; 
  gint axis_height() const;

  // Making it proportional to the drawing area means different axes have
  //  different tick sizes, which looks dumb. But it should vary a little
  //  with the size of the plot I think. Maybe check the graphing book.
  gint major_tick_size() {
#if 0
    gint dimension = is_northsouth(align_) ? height_ : width_;
    return MAX(min_minor_tick_size_, dimension/4);
#endif
    return min_major_tick_size_;
  }
  gint minor_tick_size() {
#if 0
    gint dimension = is_northsouth(align_) ? height_ : width_;
    return MAX(min_minor_tick_size_, dimension/8);
#endif
    return min_minor_tick_size_;
  }

protected:

  // Some of this stuff should probably be private so derived classes
  //  don't screw it up.

  // State used internally
  Plot_Transform trans_;

  // State set by the user.
  alignment_t align_;
  Plot_Draw* target_;
  gdouble min_, max_;

  gint inset_;


  void setup_screen_bounds();

  // Static constants
  static const gint spacing_; /* How far the numbers are from the ticks, and the
				 axis label from the numbers */
  static const gint min_major_tick_size_; // Minimum size of a major tick
  static const gint min_minor_tick_size_; // for minor

  // Static functions
  static bool is_northsouth(alignment_t a) {
    return (a == NORTH || a == SOUTH);
  }
};


#endif // _INC_AXIS_BASE_H


