// This is -*- C++ -*-
// $Id$

/* 
 * plot_data.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef _INC_PLOT_DATA_H
#define _INC_PLOT_DATA_H

#include <unistd.h> // need to define size_t... grr...

// A simple, uni-directional STL-style iterator
class Plot_Data_Const_Iterator {
public:
  Plot_Data_Const_Iterator() : current_(0), pos_(0), stride_(0) { }
  Plot_Data_Const_Iterator(const double* c, size_t pos, int s) :
    current_(c), pos_(pos), stride_(s)
    { }

  Plot_Data_Const_Iterator(const Plot_Data_Const_Iterator& pdi) :
    current_(pdi.current_), pos_(pdi.pos_), stride_(pdi.stride_)
    { }

  const double* underlying_pointer() const { return current_; }
  size_t underlying_position() const { return pos_; }
  size_t underlying_stride() const { return stride_; }

  double operator*() const { return *current_; }

  Plot_Data_Const_Iterator& operator++() {
    current_ = (double*)(((char*)current_)+stride_);
    ++pos_;
    return *this;
  }

  Plot_Data_Const_Iterator operator++(int) {
    Plot_Data_Const_Iterator save(*this);
    operator++();
    return save;
  }

  Plot_Data_Const_Iterator& operator--() {
    current_ = (double*)(((char*)current_)-stride_);
    --pos_;
    return *this;
  }

  Plot_Data_Const_Iterator operator--(int) {
    Plot_Data_Const_Iterator save(*this);
    operator--();
    return save;
  }    
   
  Plot_Data_Const_Iterator& operator+=(int n) {
    current_ = (double*)(((char*)current_)+n*stride_);
    pos_ += n;
    return *this;
  }

  Plot_Data_Const_Iterator& operator-=(int n) {
    current_ = (double*)(((char*)current_)-n*stride_);
    pos_ -= n;
    return *this;
  }

  Plot_Data_Const_Iterator& sync(const Plot_Data_Const_Iterator& t) {
    int delta = t.underlying_position() - underlying_position();
    return operator+=(delta);
  }

  // These comparison operators look only at the value of the index,
  // so comparing iterators from different data objects can get strange
  // if you don't know what you are doing...
  bool operator==(const Plot_Data_Const_Iterator& x) const {
    return pos_ == x.pos_;
  }
  bool operator!=(const Plot_Data_Const_Iterator& x) const {
    return pos_ != x.pos_;
  }
  bool operator<(const Plot_Data_Const_Iterator& x) const {
    return pos_ < x.pos_;
  }
  bool operator>(const Plot_Data_Const_Iterator& x) const {
    return pos_ > x.pos_;
  }
  bool operator<=(const Plot_Data_Const_Iterator& x) const {
    return pos_ <= x.pos_;
  }
  bool operator>=(const Plot_Data_Const_Iterator& x) const {
    return pos_ >= x.pos_;
  }

private:
  const double* current_;
  size_t pos_;
  int stride_;
};

class Plot_Data {
public:
  typedef Plot_Data_Const_Iterator const_iterator;

  Plot_Data(const double* a, size_t N) :
    base_(a), length_(N), stride_(sizeof(double)), sorted_(false),
    begin_(base_,0,stride_), end_(base_,length_,stride_)
    { }

  Plot_Data(const double* a, size_t N, int s) :
    base_(a), length_(N), stride_(s), sorted_(false),
    begin_(base_,0,stride_), end_(base_,length_,stride_)
    { }

  size_t size() const { return length_; }

  bool sorted() const { return sorted_; }
  void set_sorted(bool x = true) { sorted_ = x; }
  void set_sorted_automatic();

  const const_iterator& begin() const { return begin_; }
  const const_iterator& end() const { return end_; }
  const_iterator sync(const const_iterator& i) const;

  bool find_bounding_iterators(double min, double max,
			       const_iterator& b, const_iterator& e) const;

private:
  double lookup(size_t i) const { return *(double*)(((char*)base_)+i*stride_); }

  const double* base_;
  size_t length_;
  int stride_;
  bool sorted_;
  const_iterator begin_, end_;
};

void
synchronize(Plot_Data::const_iterator& a0, Plot_Data::const_iterator& a1,
	    Plot_Data::const_iterator& b0, Plot_Data::const_iterator& b1);

#endif // _INC_PLOT_DATA_H

// $Id$
