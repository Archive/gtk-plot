// This is -*- C++ -*-

/*
 * bounded.h
 *
 * Copyright (C) 1998 EMC Capital Management Inc. 
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef _INC_BOUNDED_H
#define _INC_BOUNDED_H

#include <glib.h>

class Bounded {
public:
  
  // It is best (?) to have non-degenerate bounds be the defaults.
  Bounded() : x_min_(0.0), x_max_(1.0),  y_min_(0.0), y_max_(1.0)
    {}

  virtual ~Bounded() {}

  virtual void set_x_bounds(gdouble x0, gdouble x1) {
    // This can be written more concisely with MIN/MAX,
    //  but I think this way is actually better.
    if (x0 > x1) {
      x_min_ = x1;
      x_max_ = x0;
    }
    else {
      x_min_ = x0;
      x_max_ = x1;
    }
#ifdef G_ENABLE_DEBUG
    g_print("X Bounds set: %f,%f\n", x_min_, x_max_);
#endif
  }

  virtual void set_y_bounds(gdouble y0, gdouble y1) {
    if (y0 > y1) {
      y_min_ = y1;
      y_max_ = y0;
    }
    else {
      y_min_ = y0;
      y_max_ = y1;
    }
#ifdef G_ENABLE_DEBUG
    g_print("Y Bounds set: %f,%f\n", y_min_, y_max_);
#endif
  }

  virtual void set_bounds(gdouble x0, gdouble x1, gdouble y0, gdouble y1) {
    set_x_bounds(x0, x1);
    set_y_bounds(y0, y1);
  }

  gdouble x_min() const { return x_min_; }
  gdouble y_min() const { return y_min_; }
  gdouble x_max() const { return x_max_; }
  gdouble y_max() const { return y_max_; }

  gdouble x_size() const { return x_max_ - x_min_; }
  gdouble y_size() const { return y_max_ - y_min_; }

  bool in_x_bounds(gdouble x) const { return (x >= x_min_) && (x <= x_max_); }
  bool in_y_bounds(gdouble y) const { return (y >= y_min_) && (y <= y_max_); }
  bool in_bounds(gdouble x, gdouble y) const {
    return in_x_bounds(x) && in_y_bounds(y);
  }

private: 
  gdouble x_min_, x_max_;
  gdouble y_min_, y_max_;
};

#endif // _INC_BOUNDED_H



