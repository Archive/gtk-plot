// This is -*- C++ -*-
// $Id$

/* 
 * plot_draw.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include "plot_draw.h"

#include <string.h>
#include <gdk/gdkprivate.h>
#include <X11/Xlib.h>
#include <X11/Xos.h>

Plot_Draw::Plot_Draw() : d1_(0), d2_(0), 
  xo_(0), yo_(0), w_(-1), h_(-1),
  gc_(0)
{ }

Plot_Draw::Plot_Draw(GdkDrawable* d) : d1_(d), d2_(0),
  xo_(0), yo_(0), w_(-1), h_(-1),
  gc_(0)
{
  // It's really OK for the args to be NULL, e.g. when copying
  //  the drawables from another Plot_Draw
}

Plot_Draw::Plot_Draw(GdkDrawable* d1, GdkDrawable* d2) : d1_(d1), d2_(d2),
  xo_(0), yo_(0), w_(-1), h_(-1),
  gc_(0)
{
  // ditto.
}

Plot_Draw::~Plot_Draw()
{
  // clean up our GC stack
  delete gc_;
  for(list<GC*>::iterator i=gc_stack_.begin(); i != gc_stack_.end(); ++i)
    delete *i;
}

void
Plot_Draw::set_drawable(GdkDrawable* d)
{
  if (d == 0)
    g_warning("Passed null pointer to Plot_Draw::set_drawable");
  d1_ = d;

  // When we set change our drawable, clear out any old GC cruft...
  gc_flush_stack();
}

void
Plot_Draw::set_secondary_drawable(GdkDrawable* d)
{
  if (d == 0)
    g_warning("Passed null pointer to Plot_Draw::set_drawable");
  d2_ = d;
}

gint
Plot_Draw::width()
{
  g_return_val_if_fail((w_ > 0) || (d1_ != 0), 0);
  return w_ > 0 ? w_ : ((GdkWindowPrivate*)drawable())->width;
}

gint
Plot_Draw::height()
{
  g_return_val_if_fail((h_ > 0) || (d1_ != 0), 0);
  return h_ > 0 ? h_ : ((GdkWindowPrivate*)drawable())->height;
}

void
Plot_Draw::gc_save()
{
  if (gc_) {
    gc_stack_.push_back(gc());
    gc_ = 0;
  }
}

void
Plot_Draw::gc_clone()
{
  GC* clone = new GC(*gc());
  gc_save();
  gc_ = clone;
}

void
Plot_Draw::gc_restore()
{
  if (gc_)
    delete gc_;
  gc_ = 0;
  if (!gc_stack_.empty()) {
    gc_ = gc_stack_.back();
    gc_stack_.pop_back();
  }
}

void
Plot_Draw::gc_flush_stack()
{
  do { gc_restore(); } while (!gc_stack_.empty());
}

void 
Plot_Draw::gc_push(GC* pushme)
{
  gc_save();
  gc_ = pushme;
}

Plot_Draw::GC*
Plot_Draw::gc_pop()
{
  GC* popme = gc();
  gc_restore();
  return popme;
}


GdkPixmap*
Plot_Draw::copy_rectangle_to_pixmap(gint x, gint y, gint w, gint h, 
				   GC* GCx)
{
  if (GCx == 0) GCx = gc();
  GdkPixmap* pix = gdk_pixmap_new(drawable(), w, h, -1);
  gdk_draw_pixmap(pix, GCx->gc(), drawable(), x, y, 0, 0, w, h);
  return pix;
}

void
Plot_Draw::paste_pixmap(GdkPixmap* pix, gint x, gint y, GC* GCx)
{
  if (GCx == 0) GCx = gc();
  gdk_draw_pixmap(drawable(), GCx->gc(), pix, 0, 0, x, y,
		  ((GdkWindowPrivate*)pix)->width,
		  ((GdkWindowPrivate*)pix)->height);

  if (secondary_drawable())
    gdk_draw_pixmap(secondary_drawable(), GCx->gc(), pix, 0, 0, x, y,
		    ((GdkWindowPrivate*)pix)->width,
		    ((GdkWindowPrivate*)pix)->height);

}

void
Plot_Draw::draw_aligned_string(gint x, gint y, const gchar* str,
			      gint x_align, gint y_align,
			      gint x_adjust, gint y_adjust,
			      GC* GCx)
{
  if (GCx == 0) GCx = gc();

  gint tw = GCx->string_width(str);
  if (x_align < 0)
    x -= tw + x_adjust;
  else if (x_align == 0)
    x -= tw/2;
  else
    x += x_adjust;

  gint th = GCx->font_height();
  if (y_align == 0)
    y += th/2;
  else if (y_align < 0)
    y -= y_adjust;
  else
    y += th + y_adjust;

  draw_string(x,y,str,GCx);
}

void
Plot_Draw::draw_text(gint x, gint y, const gchar* str, gint text_align,
		    GC* GCx)
{
  if (GCx == 0) GCx = gc();
  gint h = GCx->font_height();
  gchar* ss = g_strdup(str);
  gchar* ss_orig = ss;

  gint w=0;
  if (text_align >= 0) 
    w = GCx->text_width(str);

  while (*ss) {
    gchar* p = ss;
    while (*p && *p != '\n') ++p;
    bool more_to_do = *p;
    *p = '\0';

    gint fudge = 0;
    if (text_align >= 0) {
      gint lw = GCx->string_width(ss);
      if (text_align > 0)
	fudge = w - lw;
      else
	fudge = (w - lw)/2;
    }

    draw_string(x + fudge, y, ss, GCx);
    y += h;
    ss = p;
    if (more_to_do) ++ss;
  }

  g_free(ss_orig);
}

void
Plot_Draw::draw_aligned_text(gint x, gint y, const gchar* str,
			    gint text_align,
			    gint x_align, gint y_align,
			    gint x_adjust, gint y_adjust,
			    GC* GCx)
{
  if (GCx == 0) GCx = gc();
  
  gint tw = GCx->text_width(str);
  if (x_align < 0)
    x -= tw + x_adjust;
  else if (x_align == 0)
    x -= tw/2;
  else
    x += x_adjust;

  y += GCx->font_height();
  gint th = GCx->text_height(str);
  if (y_align == 0)
    y -= th/2;
  else if (y_align < 0)
    y -= th + y_adjust;
  else
    y += y_adjust;

  draw_text(x,y,str,text_align,GCx);
}


////////////////////////////// GC stuff

Plot_Draw::GC::GC(GdkWindow* win) : window_(win), gc_(0)
{ }

Plot_Draw::GC::GC(GtkWidget* widget) :
  window_(widget->window), gc_(0)
{ }

Plot_Draw::GC::GC(GdkWindow* win, GdkGC* mygc) : 
  window_(win), gc_(mygc)
{
  gdk_gc_ref(gc_);
}

Plot_Draw::GC::GC(const GC& mygc) : window_(mygc.window_), gc_(0)
{
  if (mygc.gc_)
    gdk_gc_copy(gc_ = gdk_gc_new(window_), mygc.gc_);
}

Plot_Draw::GC::~GC()
{
  if (gc_)
    gdk_gc_unref(gc_);
}

static void
calc_pixel(GdkColor* c)
{
  gdk_color_alloc(gdk_colormap_get_system(), c);
}

void 
Plot_Draw::GC::set_foreground(const char* s)
{
  GdkColor c;
  if (gdk_color_parse(s, &c))
    calc_pixel(&c);
  else return;
  
  set_foreground(&c);
}

void 
Plot_Draw::GC::set_background(const char* s)
{
  GdkColor c;
  if (gdk_color_parse(s, &c))
    calc_pixel(&c);
  else return;

  set_background(&c);
}

GdkFont*
Plot_Draw::GC::font()
{
  GdkGCValues val;
  gdk_gc_get_values(gc(), &val);
  if (val.font == 0) {
    set_font("fixed");
    return font();  // If we don't have "fixed", this could get ugly.
  }
  return val.font;
}

gint
Plot_Draw::GC::text_width(const gchar* s)
{
  gchar* ss = g_strdup(s);
  gchar* ss_orig = ss;

  gint width_max = 0;

  while (*ss) {
    gchar* p = ss;
    while (*p && *p != '\n') ++p;
    bool more_to_do = *p;
    *p = '\0';
    gint w = string_width(ss);
    if (w > width_max)
      width_max = w;
    ss = p;
    if (more_to_do) ++ss;
  }
 
  free(ss_orig);

  return width_max;
}

gint
Plot_Draw::GC::text_height(const gchar* s)
{
  gint lines=0;
  while (*s) {
    ++lines;
    while (*s && *s != '\n') ++s;
    if (*s) ++s;
  }

  return lines * font_height();
}

void
Plot_Draw::GC::set_line_width(gint line_width)
{
  GdkGCPrivate* gc_private = (GdkGCPrivate*)gc();
  XGCValues xvalues;
  xvalues.line_width = line_width;
  XChangeGC(gc_private->xdisplay, gc_private->xgc, GCLineWidth, &xvalues);
}

void
Plot_Draw::GC::set_line_style(GdkLineStyle line_style)
{
  GdkGCPrivate* gc_private = (GdkGCPrivate*)gc();
  XGCValues xvalues;
  xvalues.line_style = line_style;
  XChangeGC(gc_private->xdisplay, gc_private->xgc, GCLineStyle, &xvalues);
}

void
Plot_Draw::GC::set_cap_style(GdkCapStyle cap_style)
{
  GdkGCPrivate* gc_private = (GdkGCPrivate*)gc();
  XGCValues xvalues;
  xvalues.cap_style = cap_style;
  XChangeGC(gc_private->xdisplay, gc_private->xgc, GCCapStyle, &xvalues);
}

void
Plot_Draw::GC::set_join_style(GdkJoinStyle join_style)
{
  GdkGCPrivate* gc_private = (GdkGCPrivate*)gc();
  XGCValues xvalues;
  xvalues.join_style = join_style;
  XChangeGC(gc_private->xdisplay, gc_private->xgc, GCJoinStyle, &xvalues);
}

void
Plot_Draw::GC::set_dashes(gint dash_offset, char* dash_list, gint dash_list_len) {
  GdkGCPrivate* gc_private = (GdkGCPrivate*)gc();
  XSetDashes(gc_private->xdisplay, gc_private->xgc,
	     dash_offset, dash_list, dash_list_len);
}

void
Plot_Draw::GC::set_dashes(gint size)
{
  char d[2] = { size, size };
  set_dashes(0,d,2);
}

void
Plot_Draw::GC::set_dashes(gint on, gint off)
{
  char d[2] = { on, off };
  set_dashes(0,d,2);
}


// $Id$
