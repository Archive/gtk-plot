// This is -*- C++ -*-
// $Id$

//
// Confidential Proprietary Material Exclusively Owned by EMC Capital
// Management.  (C) EMC Capital Management 1998.  All Rights Reserved.
//

#include "plot_data.h"

void
Plot_Data::set_sorted_automatic()
{
  sorted_ = true;
  if (length_ < 2) 
    return;

  const_iterator i = begin();
  double prev = *i;
  ++i;
  while (i != end() && sorted_) {
    double x = *i;
    if (x < prev)
      sorted_ = false;
    prev = x;
  }
}

Plot_Data::const_iterator
Plot_Data::sync(const const_iterator& i) const
{
  size_t p = i.underlying_position();
  return const_iterator((double*)(((char*)base_)+stride_*p), p, stride_);
}

bool
Plot_Data::find_bounding_iterators(double min, double max,
				   const_iterator& beg,
				   const_iterator& end) const
{
  if (!sorted()) {
    beg = begin();
    end = this->end(); // we must resolve this due to my stupid var names
    return false;
  }

  if (min > max) {
    double t = min;
    min = max;
    max = min;
  }

  double bottom = base_[0];
  double top = lookup(length_-1);

  if (max < bottom || min > top) {
    beg = end = this->end(); // see above
    return true;
  }

  size_t a, b;
  size_t i=0, j=length_-1;

  if (min <= bottom)
    a = 0;
  else {
    for(;;) {
      size_t mid = (i+j)/2;
      if (mid == length_-1)
	--mid;
      double x = lookup(mid);
      if (x < min && min <= lookup(mid+1)) {
	a = mid+1;
	break;
      } else if (mid == i || mid == j) {
	beg = end = this->end();
	return true;
      } else if (x >= min)
	j = mid;
      else
	i = mid;
    }
  }

  i=a; j=length_-1;
  
  if (max >= top)
    b = length_;
  else {
    for(;;) {
      size_t mid=(i+j)/2;
      if (mid == length_-1)
	--mid;
      double x = lookup(mid);
      if (x <= max && max < lookup(mid+1)) {
	b = mid+1;
	break;
      } else if (mid == i || mid == j) {
	beg = end = this->end();
	return true;
      } else if (x > max)
	j = mid;
      else
	i = mid;
    }
  }

  beg = const_iterator((double*)(((char*)base_)+a*stride_), a, stride_);
  end = const_iterator((double*)(((char*)base_)+b*stride_), b, stride_);
  return true;
}

void
synchronize(Plot_Data::const_iterator& a0, Plot_Data::const_iterator& a1,
	    Plot_Data::const_iterator& b0, Plot_Data::const_iterator& b1)
{
  if (a0.underlying_position() < b0.underlying_position())
    a0.sync(b0);
  else if (a0.underlying_position() > b0.underlying_position())
    b0.sync(a0);

  if (a1.underlying_position() < b1.underlying_position())
    b1.sync(a1);
  else if (a1.underlying_position() > b1.underlying_position())
    a1.sync(b1);
}


// $Id$
