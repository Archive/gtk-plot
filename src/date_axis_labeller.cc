// This is -*- C++ -*-
// $Id$

/* 
 * date_axis_labeller.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include "date_axis_labeller.h"

static void clear_rows(vector<Date_Axis_Labeller::Row*> & rows)
{
  vector<Date_Axis_Labeller::Row*>::iterator i = rows.begin();
  while (i != rows.end()) 
    {
      delete *i;
      ++i;
    }
  
  rows.clear();
}

void 
Date_Axis_Labeller::Settings::clear()
{
  clear_rows(rows);
}

void 
Date_Axis_Labeller::StrftimeRow::draw(Date_Axis_Labeller& dal,
                                      Axis_Base* primary,
                                      G_Date& date_min, 
                                      G_Date& date_max, 
                                      gint    pixel_min,
                                      gint    pixel_max,
                                      gint adjust)
{
  guint32 days_size = date_max.julian() - date_min.julian();

  g_return_if_fail(days_size > 0);

  Plot_Draw* draw = primary->drawing_target();

  g_return_if_fail(draw != 0);
      
  vector<G_Date> major_ticks;
  vector<G_Date> minor_ticks;
  vector<G_Date> labels;
  vector<G_Date> label_positions; // for centering between ticks, this is different from labels

  if (center_ == false) 
    {  
      G_Date counter = date_min;
      get_starting_date(u_, counter);

      while (counter <= date_max) {
        
        // get_starting_date possibly returns a too-low number to catch all 
        //  minor ticks.
        
        if (counter >= date_min)
          {
            if (major_ticks_) major_ticks.push_back(counter);
            else if (minor_ticks_) minor_ticks.push_back(counter);
            labels.push_back(counter);
            label_positions.push_back(counter);
          }
        
        
        G_Date tmp = counter;
        
        step(u_,counter);

        // If we did major ticks, then put the minor ticks
        //  in between them.
        if (major_ticks_)
          {
            guint stepsize = counter.julian() - tmp.julian();
            g_return_if_fail(stepsize > 0);
            guint half = stepsize/2;
            if (half > 0) {
              tmp.add_days(half);
              if (tmp >= date_min && tmp <= date_max) 
                {
                  if (minor_ticks_) minor_ticks.push_back(tmp);
                }
            }
          }
      }
    }
  else 
    {
      G_Date counter = date_min;
      get_starting_date(u_, counter);
      
      while (counter <= date_max) {
        
        // get_starting_date possibly returns a too-low number to catch all 
        //  minor ticks.
        
        if (counter >= date_min)
          {
            if (major_ticks_) major_ticks.push_back(counter);
            else if (minor_ticks_) minor_ticks.push_back(counter);
          }
        labels.push_back(counter);
        
        G_Date tmp = counter;
        
        step(u_,counter);
        
        guint stepsize = counter.julian() - tmp.julian();
        g_return_if_fail(stepsize > 0);
        guint half = stepsize/2;
        if (half > 0) {
          tmp.add_days(half);
          if (tmp >= date_min && tmp <= date_max) 
            {
              if (major_ticks_ && minor_ticks_) minor_ticks.push_back(tmp);
              label_positions.push_back(tmp);
            }
        }
        if (labels.size() > label_positions.size())
          labels.pop_back(); // couldn't get a good label position.
      }

      if (labels.empty())
        {
          g_return_if_fail(label_positions.empty());

          // none were on-screen so do the best we can
          counter = date_min;
          get_starting_date(u_,counter);
          G_Date early = counter;

          // We depend on the fact that date_min is within the step
          // interval for this unit which begins at early, since
          // otherwise get_starting_date would have returned a later
          // step interval start.
          labels.push_back(early);
          label_positions.push_back(date_min);

          // If there's another unit step on screen, label it.
          step(u_,counter);
          G_Date late = counter;

          if (late <= date_max)
            {
              labels.push_back(late);
              label_positions.push_back(late);
            }
        }
    }


  g_return_if_fail(labels.size() == label_positions.size());

  // Now we draw, major ticks, minor ticks, labels


  vector<G_Date>::iterator j = major_ticks.begin();
  
  while (j != major_ticks.end()) 
    {
      gdouble t = static_cast<gdouble>(j->julian());
      
      dal.draw_major_tick(t);

      ++j;
    }

  j = minor_ticks.begin();
  
  while (j != minor_ticks.end()) 
    {
      gdouble t = static_cast<gdouble>(j->julian());

      dal.draw_minor_tick(t);
      
      ++j;
    }  

  j = labels.begin();
  vector<G_Date>::iterator k = label_positions.begin();
  while (j != labels.end()) 
    {
      // use position date
      gdouble t = static_cast<gdouble>(k->julian());
      
      gchar buf[256];
      
      // use label date
      j->strftime(buf,255,format_.c_str());
      
      // left-align unless it's a superlabel centered between ticks
      //      dal.draw_label(t,adjust,buf,super_,(super_ && !center_));
      dal.draw_label(t,adjust,buf,super_,center_);

      ++j;
      ++k;
    }


}

void 
Date_Axis_Labeller::label()
{
  if (automagical_) supercalifragilisticexpialidocious();

  if (settings_.rows.empty()) return;

  gdouble min = primary()->min();
  gdouble max = primary()->max();
  gdouble size = max - min;

  gint pixel_min = primary()->transform()->transform(min);
  gint pixel_max = primary()->transform()->transform(max);
  gint pixel_size = pixel_max - pixel_min;

  G_Date date_min(static_cast<guint32>(min));
  g_return_if_fail(date_min.valid());

  G_Date date_max(static_cast<guint32>(max));
  g_return_if_fail(date_max.valid());

  g_return_if_fail(date_max > date_min);

  Plot_Draw* draw = primary()->drawing_target();

  g_return_if_fail(draw != 0);

  guint row = 0;
  guint row_base = primary()->major_tick_size() + 1;
  guint row_height = draw->gc()->font_height(); // FIXME for vertical

  vector<Row*>::iterator i = settings_.rows.begin();
  while (i != settings_.rows.end())
    {
      (*i)->draw(*this,primary(),
                 date_min,date_max,
                 pixel_min,pixel_max,
                 row_base + row*row_height);
      
      ++row;
      ++i;
    }

}

/* This requires some explanation. Basically what we want is: the
 * total number of days, divided by the number of tick marks,
 * should match the number of days in our chosen Units. The problem
 * that gets us all screwed is this: the gap between each tick mark is
 * ideally just enough to permit non-overlapping labels. But, for each
 * Unit, the label size is different; moreover, each Unit can be
 * labelled in more than one way. So in order to choose Units, we need
 * the tick-mark-gap, but in order to get tick-mark-gap, we need
 * Units. (tick-mark-gap gives us the number of tick marks)
 *
 * So. We are basically doomed, and really either tick-gaps or units 
 * should just be set by the user. And we do permit that. But we will 
 * also try to be smart.
 * 
 * The algorithm is brain-damaged: we check whether we have a nice
 * match for each unit with its preferred pixel gap size. If so,
 * we use that unit. If we get no matches, we pick the closest.
 *
 * Since it's slow, optimize later.
 */
  
static inline double 
closeness(double candidate, double target, double fudge)
{
  g_return_val_if_fail(fudge > 0.0, 100000000000.0);
  if ((candidate > (target - fudge)) && (candidate < (target + fudge)))
    return 0.0;
  else 
    return fabs(candidate - target);
}

static bool
match(const G_Date& scratch,
      Plot_Draw::GC& gc, 
      guint ndays,
      gint npixels,
      const gchar* format, 
      double target, 
      double fudge,
      double & best_so_far)
{
  if (best_so_far == 0.0) return false; // this is sort of a hack, since we should just not call match in this case

  gchar buf[256];
  scratch.strftime(buf,255,format);
  gint pertick = gc.string_width(buf) + 17; // 17 corrects for assorted uncertainties
  if (pertick == 0) return false;  // pure paranoia
  gint nticks  = npixels/pertick;
  if (nticks == 0) return false;

  double days = double(ndays)/double(nticks); // this is the days per tick

  g_print("%d ticks, %g days/tick\n", nticks, days);

  double score = closeness(days, target, fudge);
  if (score < best_so_far) {
    best_so_far = score;
    return true;
  }
  else return false;
}

bool
Date_Axis_Labeller::is_a_nice_match(Plot_Draw & draw, 
                                    Units unit,
                                    guint ndays,
                                    gint  npixels,
                                    vector<Row*> & rows,
                                    double & best_so_far)
{
  G_Date scratch(30, G_DATE_SEPTEMBER, 2004); // "wide" date, in English anyway
  Plot_Draw::GC & gc = *draw.gc();

  switch (unit) {
  case Days:
    {
      static const double days_fudge = 0.99;
      static const double days_target = 1.0;

      bool found = false;

      if (match(scratch,gc,ndays,npixels," %A %d ", days_target, days_fudge, best_so_far))
        {
          clear_rows(rows);

          Row* r = new StrftimeRow(Days,string(" %A %d "),false,false,false,true);
          rows.push_back(r);

          found = true;
        }

      if (match(scratch,gc,ndays,npixels," %a %d ", days_target, days_fudge, best_so_far))
        {
          clear_rows(rows);

          Row* r = new StrftimeRow(Days,string(" %a %d "),false,false,false,true);
          rows.push_back(r);

          found = true;
        }
        

      if (match(scratch,gc,ndays,npixels," %d ", days_target, days_fudge, best_so_far))
        {
          clear_rows(rows);

          Row* r = new StrftimeRow(Days,string(" %d "),false,false,false,true);
          rows.push_back(r);

          found = true;
        }

      if (found) 
        {
          Row* r = new StrftimeRow(Months,string("%B"),true,true,true,false);
          rows.push_back(r);
          r = new StrftimeRow(Years,string("%Y"),true,true,true,false);
          rows.push_back(r);
        }
      
      return found;
    }
    break;


  case TwoDays:
    {
      static const double twodays_fudge = 2.0; // note that we check 1 day first
      static const double twodays_target = 2.0;

      bool found = false;

      if (match(scratch,gc,ndays,npixels," %d ", twodays_target, twodays_fudge, best_so_far))
        {
          clear_rows(rows);

          Row* r = new StrftimeRow(TwoDays,string(" %d "),false,false,false,true);
          rows.push_back(r);

          found = true;
        }

      if (found) 
        {
          Row* r = new StrftimeRow(Months,string("%B"),true,true,true,false);
          rows.push_back(r);
          r = new StrftimeRow(Years,string("%Y"),true,true,true,false);
          rows.push_back(r);
        }
      
      return found;
    }
    break;


  case Weeks:
    {
      static const double weeks_fudge = 3.0;
      static const double weeks_target = 7.0;

      bool found = false;

      if (match(scratch,gc,ndays,npixels," %U ", weeks_target, weeks_fudge, best_so_far))
        {
          clear_rows(rows);

          Row* r = new StrftimeRow(Weeks,string(" %U "),false,false,false,true);
          rows.push_back(r);

          found = true;
        }

      if (found) 
        {
          Row* r = new StrftimeRow(Months,string("%B"),true,true,true,false);
          rows.push_back(r);
          r = new StrftimeRow(Years,string("%Y"),true,true,true,false);
          rows.push_back(r);
        }
      
      return found;
    }
    break;


  case TwoWeeks:
    {
      static const double twoweeks_fudge = 6.0;
      static const double twoweeks_target = 14.0;

      bool found = false;

      if (match(scratch,gc,ndays,npixels," %U ", twoweeks_target, twoweeks_fudge, best_so_far))
        {
          clear_rows(rows);

          Row* r = new StrftimeRow(TwoWeeks,string(" %U "),false,false,false,true);
          rows.push_back(r);

          found = true;
        }

      if (found) 
        {
          Row* r = new StrftimeRow(Months,string("%B"),true,true,true,false);
          rows.push_back(r);
          r = new StrftimeRow(Years,string("%Y"),true,true,true,false);
          rows.push_back(r);
        }
      
      return found;      
    }
    break;


  case Months:
    {
      static const double months_fudge = 12.0;
      static const double months_target = 30.0;

      bool found = false;

      if (match(scratch,gc,ndays,npixels," %B ", months_target, months_fudge, best_so_far))
        {
          clear_rows(rows);

          Row* r = new StrftimeRow(Months,string(" %B "),false,false,false,true);
          rows.push_back(r);

          found = true;
        }

      if (match(scratch,gc,ndays,npixels," %b ", months_target, months_fudge, best_so_far))
        {
          clear_rows(rows);

          Row* r = new StrftimeRow(Months,string(" %b "),false,false,false,true);
          rows.push_back(r);

          found = true;
        }

      if (match(scratch,gc,ndays,npixels," %m ", months_target, months_fudge, best_so_far))
        {
          clear_rows(rows);

          Row* r = new StrftimeRow(Months,string(" %m "),false,false,false,true);
          rows.push_back(r);

          found = true;
        }

      if (found) 
        {
          Row* r = new StrftimeRow(Years,string("%Y"),true,true,true,false);
          rows.push_back(r);
        }
      
      return found;      

    }
    break;


  case ThreeMonths:
    {
      static const double threemonths_fudge = 30.0;
      static const double threemonths_target = 91.0;

      bool found = false;

#if 0
      if (match(scratch,gc,ndays,npixels," %B-%B ", threemonths_target, threemonths_fudge, best_so_far))
        {
          clear_rows(rows);

          Row* r = new StrftimeRow(ThreeMonths,string(" %B-%B "),false,true,false,true);
          rows.push_back(r);

          found = true;
        }
#endif
      if (match(scratch,gc,ndays,npixels," %b-%b ", threemonths_target, threemonths_fudge, best_so_far))
        {
          clear_rows(rows);

          Row* r = new StrftimeRow(ThreeMonths,string(" %b-%b "),false,true,false,true);
          rows.push_back(r);

          found = true;
        }


      if (match(scratch,gc,ndays,npixels,"%b  ", threemonths_target, threemonths_fudge, best_so_far))
        {
          clear_rows(rows);

          Row* r = new StrftimeRow(ThreeMonths,string("%b  "),false,false,false,true);
          rows.push_back(r);

          found = true;
        }

#if 0
      if (match(scratch,gc,ndays,npixels," %m ", threemonths_target, threemonths_fudge, best_so_far))
        {
          clear_rows(rows);

          Row* r = new StrftimeRow(ThreeMonths,string(" %m "),false,true,false,true);
          rows.push_back(r);

          found = true;
        }
#endif

      if (found) 
        {
          Row* r = new StrftimeRow(Years,string("%Y"),true,true,true,false);
          rows.push_back(r);
        }
      
      return found;      
    }
    break;

  case Years:
    {
      static const double years_fudge = 180.0; // big since the threemonths-years gap is big
      static const double years_target = 365.0;

      bool found = false;

      if (match(scratch,gc,ndays,npixels," %Y ", years_target, years_fudge, best_so_far))
        {
          clear_rows(rows);

          Row* r = new StrftimeRow(Years,string(" %Y "),false,false,false,true);
          rows.push_back(r);

          found = true;
        }

      if (match(scratch,gc,ndays,npixels," %y ", years_target, years_fudge, best_so_far))
        {
          clear_rows(rows);

          Row* r = new StrftimeRow(Years,string(" %y "),false,false,false,true);
          rows.push_back(r);

          // Show centuries, since we have two-digit years
          r = new StrftimeRow(Centuries,string("%Ys"),true,true,true,false);
          rows.push_back(r);

          found = true;
        }
      
      return found;
    }
    break;


  case TwoYears:
    {
      static const double twoyears_fudge = 365.0;
      static const double twoyears_target = 730.0;

      bool found = false;

      if (match(scratch,gc,ndays,npixels," %Y ", twoyears_target, twoyears_fudge, best_so_far))
        {
          clear_rows(rows);

          Row* r = new StrftimeRow(TwoYears,string(" %Y "),false,false,false,true);
          rows.push_back(r);

          found = true;
        }

      if (match(scratch,gc,ndays,npixels," %y ", twoyears_target, twoyears_fudge, best_so_far))
        {
          clear_rows(rows);

          Row* r = new StrftimeRow(TwoYears,string(" %y "),false,false,false,true);
          rows.push_back(r);

          // Since we have two-digit years
          r = new StrftimeRow(Decades,string("%Ys"),true,true,true,false);
          rows.push_back(r);

          found = true;
        }
      
      return found;
    }
    break;


  case FiveYears:
    {
      static const double fiveyears_fudge = 730.0;
      static const double fiveyears_target = 1826.0;

      bool found = false;

      if (match(scratch,gc,ndays,npixels," %Y ", fiveyears_target, fiveyears_fudge, best_so_far))
        {
          clear_rows(rows);

          Row* r = new StrftimeRow(FiveYears,string(" %Y "),false,false,false,true);
          rows.push_back(r);

          found = true;
        }

      if (match(scratch,gc,ndays,npixels," %y ", fiveyears_target, fiveyears_fudge, best_so_far))
        {
          clear_rows(rows);

          Row* r = new StrftimeRow(FiveYears,string(" %y "),false,false,false,true);
          rows.push_back(r);

          // Since we have two-digit years
          r = new StrftimeRow(Centuries,string("%Ys"),true,true,true,false);
          rows.push_back(r);

          found = true;
        }
      
      return found;
    }
    break;


  case Decades:
    {
      static const double decades_fudge = 1000.0;
      static const double decades_target = 3652.0;

      bool found = false;

      if (match(scratch,gc,ndays,npixels," %Y ", decades_target, decades_fudge, best_so_far))
        {
          clear_rows(rows);

          Row* r = new StrftimeRow(Decades,string(" %Y "),false,false,false,true);
          rows.push_back(r);

          found = true;
        }

      if (match(scratch,gc,ndays,npixels," %y ", decades_target, decades_fudge, best_so_far))
        {
          clear_rows(rows);

          Row* r = new StrftimeRow(Decades,string(" %y "),false,false,false,true);
          rows.push_back(r);

          // Since we have two-digit years
          r = new StrftimeRow(Centuries,string("%Ys"),true,true,true,false);
          rows.push_back(r);

          found = true;
        }
      
      return found;
    }
    break;


  case Scores:
    {
      static const double scores_fudge = 3000.0;
      static const double scores_target = 7300.0;

      bool found = false;

      if (match(scratch,gc,ndays,npixels," %Y ", scores_target, scores_fudge, best_so_far))
        {
          clear_rows(rows);

          Row* r = new StrftimeRow(Scores,string(" %Y "),false,false,false,true);
          rows.push_back(r);

          found = true;
        }

      if (match(scratch,gc,ndays,npixels," %y ", scores_target, scores_fudge, best_so_far))
        {
          clear_rows(rows);

          Row* r = new StrftimeRow(Scores,string(" %y "),false,false,false,true);
          rows.push_back(r);

          // Since we have two-digit years
          r = new StrftimeRow(Centuries,string("%Ys"),true,true,true,false);
          rows.push_back(r);

          found = true;
        }
      
      return found;
    }
    break;

    
  case HalfCenturies:
    {
      static const double halfcenturies_fudge = 8000.0;
      static const double halfcenturies_target = 18000.0;

      bool found = false;

      if (match(scratch,gc,ndays,npixels," %Y ", halfcenturies_target, halfcenturies_fudge, best_so_far))
        {
          clear_rows(rows);

          Row* r = new StrftimeRow(HalfCenturies,string(" %Y "),false,false,false,true);
          rows.push_back(r);

          found = true;
        }
      
      return found;
    }
    break;


  case Centuries:
    {
      static const double centuries_fudge = 15000.0;
      static const double centuries_target = 36525.0;

      bool found = false;

      if (match(scratch,gc,ndays,npixels," %Y ", centuries_target, centuries_fudge, best_so_far))
        {
          clear_rows(rows);

          Row* r = new StrftimeRow(Centuries,string(" %Y "),false,false,false,true);
          rows.push_back(r);

          found = true;
        }
      
      return found;
    }
    break;


  default:
    g_warning("Bad Unit in Nice Match"); // amusing nonsense
    return false;
    break;
  }

  g_assert_not_reached();
  return false;
}

void 
Date_Axis_Labeller::supercalifragilisticexpialidocious()
{
  g_return_if_fail(automagical_);

  if (primary()->vertical()) 
    {
      g_warning("vertical not implemented");
      return;
    }

  gdouble min = primary()->min();
  gdouble max = primary()->max();
  gdouble size = max - min;

  gint pixel_min = primary()->transform()->transform(min);
  gint pixel_max = primary()->transform()->transform(max);
  gint pixel_size = pixel_max - pixel_min;

  G_Date date_min(static_cast<guint32>(min));
  g_return_if_fail(date_min.valid());

  G_Date date_max(static_cast<guint32>(max));
  g_return_if_fail(date_max.valid());

  g_return_if_fail(date_max > date_min);

  guint32 days_size = date_max.julian() - date_min.julian();

  g_return_if_fail(days_size > 0);

  Plot_Draw* draw = primary()->drawing_target();

  g_return_if_fail(draw != 0);

  settings_.clear();

  bool found = false;
  double best_so_far = 100000000000000000.0; // really bad.

  int i = 0;
  while (i < static_cast<int>(UnitsEnd)) 
    {
      Units unit = static_cast<Units>(i);
      
      if (is_a_nice_match(*draw, 
                          unit,
                          days_size,
                          pixel_size,
                          settings_.rows,
                          best_so_far)) {
        if (best_so_far == 0.0) break;
      }

      ++i;
    }

  g_print("Our best match had a closeness of %g\n", best_so_far);
}



// Move start to the previous "even" date; we use previous
//  instead of next to catch a possible minor tick between date_min and 
//  the next even date
void 
Date_Axis_Labeller::get_starting_date(Units u,
                                      G_Date & start)
{
  switch (u) {
  case Days:
    ; // do nothing, any day is a day
    break;
    
  case TwoDays:
    if (start.julian() % 2 == 1) start.subtract_days(1);
    break;
    
  case Weeks:
  case TwoWeeks: // Fall thru
    {
      GDateWeekday w = start.weekday();
      if (w != G_DATE_SUNDAY) 
        {
          start.subtract_days(static_cast<int>(w));
        }
      g_return_if_fail(start.weekday() == G_DATE_SUNDAY);
    }
    break;
    
  case Months:
    start.set_day(1);
    break;
    
  case ThreeMonths:
    {
      start.set_day(1);
      int m = static_cast<int>(start.month());
      m -= 1; // go from 1, 4, 7, 10 to 0, 3, 6, 9
      int r = m % 3;
      if (r != 0) start.subtract_months(r);
    }
    break;
    
  case Years:
    start.set_day(1);
    start.set_month(G_DATE_JANUARY);
    break;
    
  case TwoYears:
    start.set_day(1);
    start.set_month(G_DATE_JANUARY);
    if (start.year() % 2 == 1) start.subtract_years(1);
    g_return_if_fail(start.year() % 2 == 0);
    break;
    
  case FiveYears:
    {
      start.set_day(1);
      start.set_month(G_DATE_JANUARY);
      int r = start.year() % 5;
      if (r != 0) start.subtract_years(r);
      g_return_if_fail(start.year() % 5 == 0);
    }
    break;
    
  case Decades:
    {
      start.set_day(1);
      start.set_month(G_DATE_JANUARY);
      int r = start.year() % 10;
      if (r != 0) start.subtract_years(r);
      g_return_if_fail(start.year() % 10 == 0);
    }
    break;
    
  case Scores:
    {
      start.set_day(1);
      start.set_month(G_DATE_JANUARY);
      int r = start.year() % 20;
      if (r != 0) start.subtract_years(r);
      g_return_if_fail(start.year() % 20 == 0);
    }
    break;
    
  case HalfCenturies:
    {
      start.set_day(1);
      start.set_month(G_DATE_JANUARY);
      int r = start.year() % 50;
      if (r != 0) start.subtract_years(r);
      g_return_if_fail(start.year() % 50 == 0);
    }
    break;
    
  case Centuries:
    {
      start.set_day(1);
      start.set_month(G_DATE_JANUARY);
      int r = start.year() % 100;
      if (r != 0) start.subtract_years(r);
      g_return_if_fail(start.year() % 100 == 0);
    }
    break;

  default:
    g_warning("Huh? weird unit in get_starting_date");
    break;
  }
}
  
void 
Date_Axis_Labeller::step(Units u,
                         G_Date & stepme)
{
  switch (u) {
  case Days:
    stepme.add_days(1);
    break;
    
  case TwoDays:
    stepme.add_days(2);
    break;
    
  case Weeks:
    stepme.add_days(7);
    break;
    
  case TwoWeeks:
    stepme.add_days(14);
    break;
    
  case Months:
    stepme.add_months(1);
    break;
    
  case ThreeMonths:
    stepme.add_months(3);
    break;
    
  case Years:
    stepme.add_years(1);
    break;
    
  case TwoYears:
    stepme.add_years(2);
    break;
    
  case FiveYears:
    stepme.add_years(5);
    break;
    
  case Decades:
    stepme.add_years(10);
    break;
    
  case Scores:
    stepme.add_years(20);
    break;
    
  case HalfCenturies:
    stepme.add_years(50);
    break;
    
  case Centuries:
    stepme.add_years(100);
    break;

 default:
   g_warning("Weird unit in step");
   break;
  }
}


// $Id$

