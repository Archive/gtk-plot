// This is -*- C++ -*-
// $Id$

/* 
 * plot_transform.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef _INC_PLOT_TRANSFORM_H
#define _INC_PLOT_TRANSFORM_H

// A bit of code to handle your coordinate transformations for you.
// Everything should be inline in the end...

#include <math.h>
#include <iostream.h>

class Plot_Transform {
public:

  // We start out with nonsensical initial values, so please set
  // your bounds before calling transform() or inverse().
  Plot_Transform() : s0_(0), s1_(1), c0_(0), c1_(1),
    pixels_per_unit_(0), b_(0)
    { }

  // The default would probably do the right thing,
  //  but this is safer and makes future expansion simpler.
  Plot_Transform & operator= (const Plot_Transform & rhs) {
    s0_ = rhs.s0_; 
    s1_ = rhs.s1_;
    c0_ = rhs.c0_;
    c1_ = rhs.c1_;
    pixels_per_unit_ = rhs.pixels_per_unit_;
    b_ = rhs.b_;
    return *this;
  }

  void set_screen_bounds(gint s0, gint s1) {
    s0_ = s0;
    s1_ = s1;
#ifdef G_ENABLE_DEBUG
    g_print("Plot_Transform: setting screen bounds to %d, %d\n", s0, s1);
#endif
    recalc_transformation_coefficients();
  }

  void set_range_bounds(gdouble c0, gdouble c1) {
    c0_ = c0;
    c1_ = c1;
#ifdef G_ENABLE_DEBUG
    g_print("Plot_Transform: setting range bounds to %f, %f\n", c0, c1);
#endif
    recalc_transformation_coefficients();
  }

  // Transform coordinates => pixels
  gint transform(gdouble c) const { return (gint)(pixels_per_unit_*c+b_+0.5); }

  // Transform pixels => coordinates
  gdouble inverse(gint s) const { return (s-b_)/pixels_per_unit_; }

  gdouble pixels_per_unit() const { return pixels_per_unit_; }
  gdouble units_per_pixel() const { return 1/pixels_per_unit_; }

private:
  void recalc_transformation_coefficients() {
    // avoid FPU exceptions and other insanity if user is dumb
    if ( fabs(c0_-c1_) < 1e-10 ) {
      pixels_per_unit_ = 0.0;
      b_ = 0.0;
    }
    else {
      pixels_per_unit_ = (s1_-s0_)/(c1_-c0_);
      b_ = -c0_ * pixels_per_unit_ + s0_;
    }
  }
    
  gint s0_, s1_;    // screen/pixel bounds
  gdouble c0_, c1_; // coordinate system range bounds

  gdouble pixels_per_unit_;
  gdouble b_; 
};

#endif // _INC_PLOT_TRANSFORM_H

// $Id$
