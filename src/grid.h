// -*- C++ -*-

/* 
 * grid.h 
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef GUPPI_GRID_H
#define GUPPI_GRID_H

#include <string.h> // memcpy, memset
#include <glib.h>  

////////////////////////////////////////
////// Poorly tested, but it looks OK after a rewrite.
////// This is designed for reasonable speed but mostly safety and workingness.
////// Someone can optimize it later or use a vector to reimplement it.


// NOTICE The Array class will bomb big time if a class has 
//  meaningful constructors/destructors. Stick to ints and pointers.
//  And the array moves its contents around in memory so don't take 
//  the address of particular elements.

#ifdef GNOME_ENABLE_DEBUG
#define ARRAY_INVARIANTS G_STMT_START { \
  if (data_ == 0 && len_ != 0) g_error("length but no data"); \
  else if (data_ != 0 && len_ == 0) g_error("data but no length"); \
  } G_STMT_END
#else
#define ARRAY_INVARIANTS
#endif

// len_ is the number of elements in the array
// data_ is a pointer to the array
// data_ == 0 iff len_ == 0
template <class T>
class Array {
public:
  explicit Array(size_t n) : data_(0), len_(0) {
    if (n > 0) {
      data_ = new T[n];
      len_ = n;
      memset(data_,0,n*sizeof(T));
    }
    ARRAY_INVARIANTS;
  }
  Array() : data_(0), len_(0) {ARRAY_INVARIANTS;}
  ~Array() { if (data_ != 0) delete [] data_; }

  T& operator[](size_t i) { 
#ifdef GNOME_ENABLE_DEBUG
    if (i >= len_) {
      g_error("Array range exceeded. index: %u size: %u",i,len_);
    }
    ARRAY_INVARIANTS;
#endif
    return data_[i];
  }

  const T& operator[](size_t i) const {
#ifdef GNOME_ENABLE_DEBUG
    if (i >= len_) {
      g_error("Array range exceeded. index: %u size: %u",i,len_);
    }
    ARRAY_INVARIANTS;
#endif
    return data_[i];
  }
  
  // erase element i and move all elements above i
  //  down one index
  void erase(size_t i);

  // insert a blank at index i
  void insert(size_t i);

  // Reserve i elements for the array, zeroing them on creation
  void reserve(size_t i);

  // clear existing elements and resize the
  //  array to newsize
  void clear(size_t newsize = 0);

  size_t size() const {  ARRAY_INVARIANTS; return len_; }
  // passing by value is faster for ints/pointers
  void push_back(T t);
  bool empty() const { return (len_ == 0); }

private:
  // Only 64 bits (on Intel), so passing/storing by value should be
  // fine.  in particular, an Array of Array.
  T* data_;
  size_t len_;

  Array(const Array&);
  Array& operator=(const Array&);
};

template <class T>
void
Array<T>::insert(size_t i)
{
#ifdef GNOME_ENABLE_DEBUG
  if (i > len_) {
    g_warning("Can't insert at index %d, since the array is only %d elements",
	      i,len_);
    return;
  }
  ARRAY_INVARIANTS;
#endif
  // note that this call increments len_
  //  (fixme this is a wasteful way to do things - copies 
  //   could be saved since we have to copy again below.)
  reserve(len_ + 1);

  if (i == len_-1) { // i.e., if i == the original len_
    // optimization for inserting at the end. (same as just reserving)
    // (C++ really should have realloc(), Stroustrup's reason for 
    //  not having it is lame)
    return;
  }
  else {
    // memmove allows overlapping dest/src
    // data_[i+1] is OK since i isn't the last element.
    memmove(&data_[i+1],&data_[i],(len_-i-1)*sizeof(T));
    data_[i] = 0;
  }

  ARRAY_INVARIANTS;
}

template <class T>
void
Array<T>::erase(size_t i)
{
#ifdef GNOME_ENABLE_DEBUG
  // This covers the len_ == 0, data_ == 0 case.
  if (i >= len_) {
    g_warning("Array::erase - Array range exceeded.");
    return;
  }
  ARRAY_INVARIANTS;
#endif

  T* tmp;

  if (len_ == 1) {
    g_assert(i == 0);
    tmp = 0;
  }
  else {
    // New array one smaller
    // (Obviously you could optimize here; no real need to create
    //  a new array)
    tmp = new T[len_-1];
    
    // optimization for removing last element. 
    if (i == (len_ - 1)) {
      memcpy(tmp,data_,(len_-1)*sizeof(T));
    }
    // This assumes memcpy will take 0 as the # of bytes to copy.
    //  I don't actually know if this is true.
    else {
      // If i is 2, we want to copy 0 and 1, or 2*sizeof(T) bytes
      memcpy(tmp,data_,i*sizeof(T)); 
      // If i is 2, we advance to tmp[2] for destination
      T* tmp2 = &tmp[i]; 
      // Advance to data_[3] for source
      T* data2 = &data_[i+1];
#ifdef GNOME_ENABLE_DEBUG
      data_[i] = 0;        // so we can tell if it gets included. 
#endif     
      // if i is 2, copy the rest of the string.
      memcpy(tmp2,data2,(len_-i-1)*sizeof(T));
      
      // Since (len_-i-1)*sizeof(T) + i*sizeof(T) == (len_-1)*sizeof(T)
      //  this demonstrably copies the correct amount of memory.
    }
  } // if (len_ == 1) else

  // Since i > 0, data_ != 0
  delete [] data_;
  data_ = tmp;
  --len_;
  ARRAY_INVARIANTS;
}

template <class T>
void
Array<T>::reserve(size_t i)
{
#ifdef GNOME_ENABLE_DEBUG
  if (i < len_) {
    g_warning("Attempt to reserve less than current");
    return;
  }
  ARRAY_INVARIANTS;
#endif
  if (i == len_) return; // nothing to do

  T* tmp = 0;
  if (i != 0) {
    tmp = new T[i];
    if (data_ != 0) {
      memcpy(tmp,data_,len_*sizeof(T));
    }
  }

  if (data_ != 0) delete [] data_;
  data_ = tmp;

  // len_ starts out as the first new element,
  //  and ends as i. Zero new elements.
  while ( len_ < i ) {
    data_[len_] = 0;
    ++len_;
  }
  ARRAY_INVARIANTS;
}

template <class T>
void
Array<T>::clear(size_t newsize)
{
  ARRAY_INVARIANTS;
  if (data_ != 0) delete [] data_;
  if (newsize > 0) {
    data_ = new T[newsize];
    // zero out.
    size_t i = 0;
    while ( i < newsize ) {
      data_[i] = 0;
      ++i;
    }
  }
  else {
    data_ = 0;
  }
  len_ = newsize;
  ARRAY_INVARIANTS;
}

template <class T>
void 
Array<T>::push_back(T t) 
{ 
  ARRAY_INVARIANTS;
  reserve(len_+1);  // increments len_
  ARRAY_INVARIANTS;
  data_[len_-1] = t;
  ARRAY_INVARIANTS;
}

//  Array partial specialization ///////////////////////

#if 0 // egcs 1.0 pukes

template <class T>
class Array<T*> : private Array<void*> {
public:
  typedef Array<void*> Base;
  
  explicit Array(size_t n) : Base(n) {}

  Array() : Base() {}

  T*& operator[](size_t i) { 
    return static_cast<T*&>(Base::operator[](i));
  }

  const T*& operator[](size_t i) const {
    return static_cast<const T*&>(Base::operator[](i));
  }

  void erase(size_t i) { Base::erase(i); }

  void insert(size_t i) { Base::insert(i); }

  void reserve(size_t i) { Base::reserve(i); }

  void clear(size_t newsize = 0) { Base::clear(newsize); }

  size_t size() const {  return Base::size(); }

  void push_back(T* t) { Base::push_back(static_cast<void*>(t)); } 
  bool empty() const { return Base::empty(); }
};

#endif

// Grid ////////////////////////////////////////////////////////////////////

#ifdef GNOME_ENABLE_DEBUG
#define GRID_INVARIANTS G_STMT_START { if (rows() > 0) {                              \
  if (data_[0]->size() != columns_) { g_error("Size of column array != columns_"); }  \
  size_t i = 0; while ( i < rows() ) { if (data_[i]->size() != columns_) {            \
  g_error("Row %u has width %u, should have %u",i,data_[i]->size(),columns_); } ++i; } } } G_STMT_END

#else
#define GRID_INVARIANTS
#endif

template <class T> 
class Grid {
public:
  Grid(size_t newrows, size_t newcolumns) : columns_(0) { 
    reserve(newrows,newcolumns);GRID_INVARIANTS;
  }
  Grid() : columns_(0) {GRID_INVARIANTS;}
  ~Grid() { clear(); } 

  Array<T>& operator[] (size_t i) {
    GRID_INVARIANTS;
    return *(data_[i]);
  }

  const Array<T>& operator[] (size_t i) const {
    GRID_INVARIANTS;
    return *(data_[i]);
  }

  void clear();

  // With no args, appends row/column
  void add_row();
  void add_row(size_t i);
  void add_column();
  void add_column(size_t i);
  
  void delete_row(size_t i);
  void delete_column(size_t i);

  void reserve(size_t rows, size_t columns);
  void shrink (size_t rows, size_t columns);

  size_t rows() const { return data_.size(); }
  size_t columns() const { 
    return columns_;
  }

private:
  Array<Array<T>* > data_;
  // This is annoying; it's needed for the case where rows() == 0
  //  and someone wants to add columns.
  size_t columns_;

  Grid(const Grid&);
  Grid& operator=(const Grid&);
};


template <class T>
void
Grid<T>::clear()
{
  GRID_INVARIANTS;
  size_t i = 0; 
  size_t n = data_.size();

  while ( i < n ) {
    data_[i]->clear();
    delete data_[i];
    ++i;
  }
  data_.clear();
  columns_ = 0;
  GRID_INVARIANTS;
}

template <class T>
void 
Grid<T>::add_column()
{
  GRID_INVARIANTS;

  add_column(columns_);

  GRID_INVARIANTS;
}

template <class T>
void 
Grid<T>::add_column(size_t j)
{
  GRID_INVARIANTS;
  size_t i = 0;
  size_t n = data_.size();

  while ( i < n ) {
    data_[i]->insert(j);
    ++i;
  }
  ++columns_;
  
  GRID_INVARIANTS;
}

template <class T>
void 
Grid<T>::add_row()
{
  GRID_INVARIANTS;
  
  data_.push_back(new Array<T>(columns_));
  GRID_INVARIANTS;
}

template <class T>
void 
Grid<T>::add_row(size_t i)
{
  GRID_INVARIANTS;

  data_.insert(i);
  data_[i] = new Array<T>(columns_);

  GRID_INVARIANTS;
}

template <class T>
void 
Grid<T>::delete_row(size_t i)
{
  GRID_INVARIANTS;
  delete data_[i];
  data_.erase(i);
  GRID_INVARIANTS;
}

template <class T>
void 
Grid<T>::delete_column(size_t i)
{
  GRID_INVARIANTS;
  g_return_if_fail(i < columns_);

  size_t j = 0;
  size_t n = rows();

  while ( j < n ) {
    data_[j]->erase(i);
    ++j;
  }
  --columns_;
  GRID_INVARIANTS;
}

template <class T>
void 
Grid<T>::reserve(size_t r, size_t c)
{
  GRID_INVARIANTS;
  size_t oldrows = rows();  
  
  data_.reserve(r);

  size_t i = 0;
  while ( i < oldrows ) {
    data_[i]->reserve(c);
    ++i;
  }

  while ( i < r ) {
    data_[i] = new Array<T>(c);
    ++i;
  }
  columns_ = c;
  GRID_INVARIANTS;
}

// This function is impressively inefficient
template <class T>
void 
Grid<T>::shrink(size_t r, size_t c)
{
  GRID_INVARIANTS;
  g_return_if_fail(r <= rows() && c <= columns());

  size_t oldrows = rows();
  size_t oldcols = columns();

  while (oldrows > r) {
    delete_row(oldrows-1);
    --oldrows;
  }
  
  while (oldcols > c) {
    delete_column(oldcols-1);
    --oldcols;
  }

  GRID_INVARIANTS;
}

/////////////////////// Grid partial specialization for pointers 

#if 0 // egcs 1.0 pukes

template <class T> 
class Grid<T*> : private Grid<void*> {
public:
  typedef Grid<void*> Base;

  Grid(size_t newrows, size_t newcolumns) : Base(newrows, newcolumns) {}
  Grid() : Base() {}

  Array<T*>& operator[] (size_t i) {
    return static_cast<Array<T*>&>(Base::operator[](i));
  }

  const Array<T*>& operator[] (size_t i) const {
    return static_cast<const Array<T*>&>(Base::operator[](i));
  }

  void clear() { Base::clear(); }

  void add_row() { Base::add_row(); }
  void add_row(size_t i) { Base::add_row(i); }
  void add_column() { Base::add_column(); }
  void add_column(size_t i) { Base::add_column(i); }
  
  void delete_row(size_t i) { Base::delete_row(i); }
  void delete_column(size_t i) { Base::delete_column(i); }

  void reserve(size_t rows, size_t columns) { Base::reserve(rows,columns); }
  void shrink (size_t rows, size_t columns) { Base::shrink(rows,columns); }

  size_t rows() const { return Base::rows(); }
  size_t columns() const { 
    return Base::columns();
  }

};

#endif

#endif







