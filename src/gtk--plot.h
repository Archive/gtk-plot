// This is -*- C++ -*-

// This file was generated by build-headers.pl on Tue Sep  1 14:26:19 1998.
// Modify it at your own peril.

#ifndef __GTKMMPLOTLIB__
#define __GTKMMPLOTLIB__

#include <gtk--plot/arraylayout.h>
#include <gtk--plot/axis_base.h>
#include <gtk--plot/axis_labeller.h>
#include <gtk--plot/date_axis_labeller.h>
#include <gtk--plot/naive_axis_labeller.h>
#include <gtk--plot/numeric_axis_labeller.h>
#include <gtk--plot/plot_area.h>
#include <gtk--plot/plot_buffer.h>
#include <gtk--plot/plot_data.h>
#include <gtk--plot/plot_draw.h>
#include <gtk--plot/plot_transform.h>
#include <gtk--plot/grid.h>
#include <gtk--plot/g_date.h>

#endif
