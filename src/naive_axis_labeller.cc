// This is -*- C++ -*-
// $Id$

/* 
 * naive_axis_labeller.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include "naive_axis_labeller.h"

void
Naive_Axis_Labeller::label()
{
  gdouble t0=primary()->min(), t1=primary()->max();

  for(gint i=0; i<count_; ++i) {
    gdouble t = t0+i*(t1-t0)/(count_-1);
    // FIXME  This shouldn't be hard wired
    draw_tick(t,5);
    draw_numeric_label(t,5,"%g",t);
  }
}


// $Id$
