// This is -*- C++ -*-
// $Id$

/* 
 * plot_draw.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef _INC_PLOT_DRAW_H
#define _INC_PLOT_DRAW_H

// This is our drawing target object. Eventually we might make one that 
//  wraps Gdk-- too, but for now we're just wrapping plain Gdk because 
//  we need the stability.

#include <gtk/gtk.h>
#include <list>

class Plot_Draw {
public:

  /* 
     We have our own GC wrapper here, avoids namespace issues.
  */
  
  class GC {
  public:
    GC(GdkWindow*);
    GC(GtkWidget*);
    // Wrap an existing GdkGC; better make sure the window corresponds.
    GC(GdkWindow*,GdkGC*); 
    GC(const GC&);
    ~GC();

    // Hacks for getting at the underlying object, if you are so inclined.
    operator GdkGC*() { return gc(); }
    GdkGC* gc() {
      if (gc_ == 0) gc_ = gdk_gc_new(window_);
      return gc_;
    }

    ////////////////////////////////////////////////////////////////////////////

    void set_foreground(GdkColor* c) {
      gdk_gc_set_foreground(gc(), c); 
    }

    void set_background(GdkColor* c) {
      gdk_gc_set_background(gc(), c);
    }

    // These auto-alloc the color in the system colormap; 
    //  not very nice, you should probably not use it.
    void set_foreground(const char* s);
    void set_background(const char* s);


    GdkFont* font();
  
    void set_font(const gchar* s) {
      gdk_gc_set_font(gc(), gdk_font_load(s));
    }

    // I find these to be convenient functions to have available
    gint string_width(const gchar* s) { 
      return gdk_string_width(font(), s);
    }

    gint font_height() {
      GdkFont* f = font();
      return f->ascent + f->descent;
    }

    gint text_width(const gchar*);
    gint text_height(const gchar*);

    // OK, back to the non-font stuff...

    void set_clip_origin(gint x, gint y) {
      gdk_gc_set_clip_origin(gc(),x,y);
    }

    void set_clip_mask(GdkBitmap* mask) {
      gdk_gc_set_clip_mask(gc(),mask);
    }

    void set_clip_rectangle(gint x, gint y, gint w, gint h) {
      GdkRectangle rect;
      rect.x = x;
      rect.y = y;
      rect.width = w;
      rect.height = h;
      gdk_gc_set_clip_rectangle(gc(), &rect);
    }

    void set_noclip() {
      gdk_gc_set_clip_mask(gc(), 0);
    }

    // Drawing Functions: GDK_COPY, GDK_INVERT, GDK_XOR
    void set_function(GdkFunction func) {
      gdk_gc_set_function(gc(), func);
    }

    // Fill Styles: GDK_SOLID, GDK_TILED, GDK_STIPPLED, GDK_OPAQUE_STIPPLED
    void set_fill_style(GdkFill myfill) {
      gdk_gc_set_fill(gc(), myfill);
    }

    void set_tile(GdkPixmap* tile) {
      gdk_gc_set_tile(gc(), tile);
    }

    void set_stipple(GdkPixmap* stip) {
      gdk_gc_set_stipple(gc(), stip);
    }
  
    void set_ts_origin(gint x, gint y) {
      gdk_gc_set_ts_origin(gc(), x, y);
    }

    // Subwindow modes: GDK_CLIP_BY_CHILDREN, GDK_INCLUDE_INFERIORS
    void set_subwindow(GdkSubwindowMode mode) { 
      gdk_gc_set_subwindow(gc(), mode);
    }

    void set_exposures(gint myexp) {
      gdk_gc_set_exposures(gc(), myexp);
    }

    void set_line_width(gint line_width);
  
    // Line Styles: GDK_LINE_SOLID, GDK_LINE_ON_OFF_DASH, GDK_LINE_DOUBLE_DASH
    void set_line_style(GdkLineStyle line_style);

    // Cap Styles: GDK_CAP_NOT_LAST, GDK_CAP_BUTT, GDK_CAP_ROUND,
    //             GDK_CAP_PROJECTING
    void set_cap_style(GdkCapStyle cap_style);

    // Join Styles: GDK_JOIN_MITER, GDK_JOIN_ROUND, GDK_JOIN_BEVEL
    void set_join_style(GdkJoinStyle join_style);

    // If you want to set everything at one, you can use this...
    void set_line_attributes(gint line_width,
                             GdkLineStyle line_style,
                             GdkCapStyle cap_style,
                             GdkJoinStyle join_style) {
      gdk_gc_set_line_attributes(gc(), line_width, line_style, 
                                 cap_style, join_style);
    }

    void set_dashes(gint dash_offset, char* dash_list, gint dash_list_length);

    // Here are the two cases that you use most of the time:
    void set_dashes(gint size); // on and off have same size
    void set_dashes(gint on, gint off);

  private:
    GdkWindow* window_;
    GdkGC* gc_;

  };


  Plot_Draw();
  Plot_Draw(GdkDrawable*);
  Plot_Draw(GdkDrawable*, GdkDrawable*);
  virtual ~Plot_Draw();

  // If you really want to reach in and fiddle with the underlying things...
  GdkDrawable* drawable() { return d1_; }
  GdkDrawable* secondary_drawable() { return d2_; }

  /*
    If you were to change between drawables with different visual types
    in midstream, all sorts of bad things could happen with your
    GC stack.  But if you do something that strange, you probably deserve
    whatever happens to you...
  */
  void set_drawable(GdkDrawable*);
  void set_secondary_drawable(GdkDrawable*);
  void set_drawable(GdkDrawable* d1, GdkDrawable* d2) {
    set_drawable(d1);
    set_secondary_drawable(d2);
  }

  void set_default_clip_rectangle(gint xo, gint yo, gint w, gint h) {
    xo_ = xo; yo_ = yo; w_ = w; h_ = h;
    // Update current GC, if we have one...
    if (gc_) gc_->set_clip_rectangle(xo_,yo_,w_,h_);
  }

  void set_default_noclip() {
    xo_ = yo_ = 0;
    w_ = h_ = -1;
    // Again, update current GC...
    if (gc_) gc_->set_noclip();
  }

  void set_default_clip_offset_from(Plot_Draw & source, 
				    gint xoffset,
				    gint yoffset) {
    // A source w/h of -1 will result in a dest w/h depending
    // on buffer size. hrrm.
    set_default_clip_rectangle(source.x_origin() + xoffset,
			       source.y_origin() + yoffset,
			       source.width(),
			       source.height());
  }

  gint x_origin() { return xo_; }
  gint y_origin() { return yo_; }
  gint width();
  gint height();

  /*
    
    We provide a "GC Stack", which I've found can make it easier to write
    programs that are well-behaved without lots of excess passing around
    and duplicating of GCs.

    The top of the stack is the "current GC", the default GC that is
    used in drawing operations if another GC isn't specified.

    The relevant class functions are:
    gc() - fetch a pointer to the current GC
    gc_save() - push a new GC with all of the standard default values
    on top of the stack
    gc_clone() - push a copy of the current GC onto the stack
    gc_restore() - pop the current GC off of the stack.  The stack can
    never be empty --- a default GC will appear as the
    current GC when you pop away the last element in the
    stack.
    gc_push() - put a given GC on top of the stack.  You are no longer
    responsible for deleting it... if you do, you'll probably
    have some sort of horrible dangling pointer segfault before
    long.
    gc_pop() - pop the top GC off of the stack, and hand it back to you.
    You are now responsible for deleting it, or you will leak
    memory!

    In many kinds of drawing routines, I like to be able to call gc_clone()
    when I enter and gc_restore() when I exit.  That way I can change
    drawing colors and such with reckless abandon, while retaining other
    information (like clipping, for example) that might have been set
    by the calling code.

  */

  // By doing this "lazy allocation", we never have to worry about
  // creating the GC before we have been realized.
  // If we've defined a default clipping rectangle, we apply that rectangle
  // to any GC that we create.
  GC* gc() {
    if (gc_ == 0) {
      gc_ = new GC(d1_);
      if (w_ > 0 && h_ > 0)
	gc_->set_clip_rectangle(xo_,yo_,w_,h_);
    }
    return gc_;
  }

  void gc_save();
  void gc_clone();
  void gc_restore();
  void gc_flush_stack();
  void gc_push(GC*);
  GC* gc_pop();
  
  void draw_point(gint x, gint y, GC* myGC = 0) {
    if (myGC == 0) myGC = gc();
    gdk_draw_point(drawable(), myGC->gc(), x, y);
    if (secondary_drawable())
      gdk_draw_point(secondary_drawable(), myGC->gc(), x, y);
  }

  void draw_line(gint x0, gint y0, gint x1, gint y1, GC* myGC = 0) {
    if (myGC == 0) myGC = gc();
    gdk_draw_line(drawable(), myGC->gc(), x0, y0, x1, y1);
    if (secondary_drawable())
      gdk_draw_line(secondary_drawable(), myGC->gc(), x0, y0, x1, y1);
  }

  void draw_points(GdkPoint* points, gint npoints, GC* myGC = 0) {
    if (myGC == 0) myGC = gc();
    gdk_draw_points(drawable(), myGC->gc(), points, npoints);
    if (secondary_drawable())
      gdk_draw_points(secondary_drawable(), myGC->gc(), points, npoints);
  }
  
  void draw_segments(GdkSegment* segs, gint nsegs, GC* myGC = 0) {
    if (myGC == 0) myGC = gc();
    gdk_draw_segments(drawable(), myGC->gc(), segs, nsegs);
    if (secondary_drawable())
      gdk_draw_segments(secondary_drawable(), myGC->gc(), segs, nsegs);
  }

  void draw_lines(GdkPoint* points, gint npoints, GC* myGC = 0) {
    if (myGC == 0) myGC = gc();
    gdk_draw_lines(drawable(), myGC->gc(), points, npoints);
    if (secondary_drawable())
      gdk_draw_lines(secondary_drawable(), myGC->gc(), points, npoints);
  }

  // draw a rectangle, specifying the (x,y) of the upper left-hand corner
  // and the width and height.
  void draw_rectangle(bool filled, gint x, gint y, gint w, gint h, 
		      GC* myGC = 0) {
    if (myGC == 0) myGC = gc();
    gdk_draw_rectangle(drawable(), myGC->gc(), filled, x, y, w, h);
    if (secondary_drawable())
      gdk_draw_rectangle(secondary_drawable(), myGC->gc(), filled, x, y, w, h);
  }

  // draw a rectangle, specifying the (x,y) of two of the opposite corners.
  void draw_rectangle_coor(bool filled, gint x0, gint y0, gint x1, gint y1,
			   GC* myGC = 0) {
    draw_rectangle(filled,
		   x0 < x1 ? x0 : x1, y0 < y1 ? y0 : y1,
		   1+abs(x1-x0), 1+abs(y1-y0), myGC);
  }

  /*
    Annoying X-ism: When specifying the angle for an arc, it has to be given
    as degrees*64.  Therefore an arc that sweeps from 0 to 180 degrees
    should have a1=0, a2=11530 (=180*64).
  */
  void draw_arc(bool filled, gint x, gint y, gint w, gint h, gint a1, gint a2,
		GC* myGC = 0) {
    if (myGC == 0) myGC = gc();
    gdk_draw_arc(drawable(), myGC->gc(), filled, x, y, w, h, a1, a2);
    if (secondary_drawable())
      gdk_draw_arc(secondary_drawable(), myGC->gc(), filled, x, y, w, h, a1, a2);
  }

  // This is usually what I want to do when I call draw_arc() anyway...
  void draw_circle(bool filled, gint x, gint y, guint r, GC* myGC = 0) {
    draw_arc(filled,x-r,y-r,2*r,2*r,0,360*64,myGC);
  }

  void draw_polygon(bool filled, GdkPoint* points, gint npoints, GC* myGC=0) {
    if (myGC == 0) myGC = gc();
    gdk_draw_polygon(drawable(), myGC->gc(), filled, points, npoints);
    if (secondary_drawable())
      gdk_draw_polygon(secondary_drawable(),
		       myGC->gc(), filled, points, npoints);
  }

  GdkPixmap* copy_rectangle_to_pixmap(gint x, gint y, gint w, gint h,
				      GC* myGC=0);

  void paste_pixmap(GdkPixmap* pix, gint x, gint y, GC* myGC = 0);

  void draw_string(gint x, gint y, const gchar* str, GC* myGC = 0) {
    if (myGC == 0) myGC = gc();

    gdk_draw_string(drawable(), myGC->font(), myGC->gc(), x, y, str);
    if (secondary_drawable())
      gdk_draw_string(secondary_drawable(), myGC->font(), myGC->gc(), x, y, str);
  }

  /*
    Normally when drawing text, the lower left corner of the first
    character is placed at the given (x,y) coordinate.  This routine
    lets you control the alignment of the text around the (x,y) coordinate.

    Alignment is specified as follows:
    If x_align < 0, the right edge of the text string is at the given x.
    If x_align > 0, the left edge of the text string is at the given x.
    if x_align = 0, the string is horizontally centered around the given x.

    If y_align < 0, the bottom edge of the text string is at the given y.
    If y_align > 0, the top edge of the text string is at the given y.
    If y_align = 0, the string is vertically centered around the given y.

    x_adjust and y_adjust allows the text to be shifted the given number
    of pixels in the direction of the alignment.  (So if x_align < 0
    and x_adjust = 10, the right edge of the text is set 10 pixels to left
    of the given x.)

  */

  void draw_aligned_string(gint x, gint y, const gchar* str,
			   gint x_align = 0, gint y_align = 0,
			   gint x_adjust = 0, gint y_adjust = 0,
			   GC* myGC = 0);

  void draw_text(gint x, gint y, const gchar* str, gint text_align = -1,
		 GC* myGC = 0);
  void draw_aligned_text(gint x, gint y, const gchar* str,
			 gint text_align = -1,
			 gint x_align = 0, gint y_align = 0,
			 gint x_adjust = 0, gint y_adjust = 0,
			 GC* myGC = 0);


private:
  GdkDrawable* d1_;
  GdkDrawable* d2_;

  gint xo_, yo_, w_, h_;
  
  GC* gc_;
  list<GC*> gc_stack_;
};



#endif // _INC_PLOT_DRAW_H

// $Id$
