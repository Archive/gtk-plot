// This is -*- C++ -*-
// $Id$

/* 
 * numeric_axis_labeller.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include <math.h>
#include "numeric_axis_labeller.h"

/*
  I first got the idea for this implementation from poking around inside
  gnuplot's source, when it made it way into an earlier (unreleased)
  chart generating program that I wrote.  It has been beat on a lot,
  though, and has gone back and forth between C++ and Perl a few times...
  -JT
*/

void
Numeric_Axis_Labeller::label()
{

  gdouble min = primary()->min();
  gdouble max = primary()->max();

  gdouble width = max-min;
  gdouble mag = ceil(log10(width/goal_));

  double start_best=0, step_best=1;
  gint pts_best=0, error_best=10000;

  const gint divisors[] = {10,5,4,2,1,0};
  gint ptr=0;
  while (divisors[ptr]) {
    gint div=divisors[ptr];
    gdouble step = pow(10,mag)/div;
    gdouble start = ceil(min/step)*step;
    gint pts = 1+(gint)floor((max-start)/step);

    if (! locked_bounds()) {
      if (fabs(start-step-min)<step/2) {
	start -= step;
	++pts;
      }
      if (fabs(start+pts*step-max) < step/2)
	++pts;
    }

    gint error = abs(pts-goal_);
    if (error < error_best) {
      start_best = start;
      step_best = step;
      pts_best = pts;
      error_best = error;
    }

    ++ptr;
  }

  if (!locked_bounds()) {
    bool changed=false;
    if (start_best < min) {
      min = start_best - step_best/10; // small padding
      changed = true;
    }
    if (max < start_best+(pts_best-1)*step_best) {
      max = start_best+(pts_best-1)*step_best + step_best/10; // small padding
      changed = true;
    }
    if (changed) {
      reset_bounds(min,max);
      g_print("Expanded bounds to %f %f\n",min,max);
    }
  }

  for(gint i=0; i<pts_best; ++i) {
    double t = start_best + step_best*i;
    // FIXME: The format, tick size, etc, shouldn't be hard-wired.
    draw_tick(t, 5);
    draw_numeric_label(t,5,"%g",t);
  }

  // FIXME: And we should also be able to do intermediate unlabelled ticks

}


// $Id$
