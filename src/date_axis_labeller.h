// This is -*- C++ -*-
// $Id$

/* 
 * date_axis_labeller.h
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef _INC_DATE_AXIS_LABELLER_H
#define _INC_DATE_AXIS_LABELLER_H

#include "axis_labeller.h"
#include "plot_draw.h"
#include "g_date.h"
#include <vector>

class Date_Axis_Labeller : public Axis_Labeller {
public:
  Date_Axis_Labeller(Axis_Base* a) : Axis_Labeller(a), automagical_(true)
    { }

  virtual ~Date_Axis_Labeller() {}

  virtual void label();

  typedef enum {
    Days,
    TwoDays,
    Weeks,
    TwoWeeks,
    Months,
    ThreeMonths,
    Years,
    TwoYears,
    FiveYears,
    Decades,
    Scores,
    HalfCenturies,
    Centuries,
    // If you need Millenia write your own damn code ;-)
    
    UnitsEnd
  } Units;

  // a row of labels to be drawn
  class Row {
  public:
    virtual ~Row() {}

    virtual void draw(Date_Axis_Labeller& dal, 
                      Axis_Base* primary, // just to get information from, not draw to directly
                      G_Date& date_min, 
                      G_Date& date_max, 
                      gint    pixel_min,
                      gint    pixel_max,
                      gint adjust) = 0;

  };

  class StrftimeRow : public Row {
  public:
    StrftimeRow(Units u, 
                string format, 
                bool super, 
                bool center, 
                bool major = false, 
                bool minor = false) : 
      u_(u), 
      format_(format),
      major_ticks_(major),
      minor_ticks_(minor),
      super_(super),
      center_(center)
      {}
    virtual ~StrftimeRow() {}

    virtual void draw(Date_Axis_Labeller& dal,
                      Axis_Base* primary,
                      G_Date& date_min, 
                      G_Date& date_max, 
                      gint    pixel_min,
                      gint    pixel_max,
                      gint adjust);

    // Default copy and assignment are correct.

  private:
    Units u_;
    string format_;
    // whether to draw each tickmark (these aren't affected by adjust)
    guint major_ticks_ : 1;
    guint minor_ticks_ : 1;
    // whether to draw in "super" style (bold)
    guint super_       : 1;
    // whether to center the label between ticks.
    guint center_      : 1;
  };

  struct Settings {

    Settings() {}

    vector<Row*> rows;
    
    // delete all rows and reset defaults
    void clear();
  };

  void set(const Settings & s) { 
    g_return_if_fail(automagical_ == false);
    settings_ = s; 
  }
  const Settings & get() { return settings_; }

  void set_automagical(bool b) { automagical_ = b; }

  static void get_starting_date(Units u,
                                G_Date & start);
  
  static void step(Units u,
                   G_Date & stepme);

private:
  bool automagical_;  // fill in the settings, don't use user settings.

  Settings settings_;

  void supercalifragilisticexpialidocious(); // do the automagicality 


  static bool is_a_nice_match(Plot_Draw & draw, 
                              Units unit,
                              guint ndays,
                              gint  npixels,
                              vector<Row*> & rows,
                              double & best_so_far);

};



#endif // _INC_DATE_AXIS_LABELLER_H

// $Id$
