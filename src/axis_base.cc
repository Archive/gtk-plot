// -*- C++ -*-

/* 
 * axis_base.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include "axis_base.h"
#include <string>

#define G_ENABLE_DEBUG

const gint Axis_Base::spacing_ = 4;
const gint Axis_Base::min_major_tick_size_ = 8;
const gint Axis_Base::min_minor_tick_size_ = 4;

gint 
Axis_Base::axis_width() const
{
  if (target_ == 0) return 0;

  if (horizontal()) {
    return target_->width() - inset_*2;
  }
  else return target_->width();
}

gint 
Axis_Base::axis_height() const
{
  if (target_ == 0) return 0;

  if (horizontal()) {
    return target_->height();
  }
  else return target_->height() - inset_*2;
}


void 
Axis_Base::setup_screen_bounds() 
{
  if (target_ == 0) return;

  else if (horizontal())
    trans_.set_screen_bounds(target_->x_origin() + inset_, 
			     target_->x_origin() + target_->width() - inset_);
  else
    trans_.set_screen_bounds(target_->y_origin() + target_->height() - inset_, 
			     target_->y_origin() + inset_);
}


void
Axis_Base::set_drawing_target(Plot_Draw* d)
{
  g_return_if_fail(d != 0);
  target_ = d;

  setup_screen_bounds();
}

void 
Axis_Base::set_axis_line_inset(gint inset)
{
  g_return_if_fail(inset > 0);
  inset_ = inset;

  setup_screen_bounds();
}

void
Axis_Base::set_bounds(gdouble a, gdouble b)
{
    if (a > b) {
      min_ = b;
      max_ = a;
    }
    else {
      min_ = a;
      max_ = b;
    }
    trans_.set_range_bounds(a,b);
}

void 
Axis_Base::draw_axis_line()
{
  g_return_if_fail(target_ != 0);

  gint x_start = 0, y_start = 0;
  gint x_stop = 0,  y_stop = 0;

  // The + or - 1 gets us inside the clipping region
  switch (align_) {
  case NORTH:
    x_start = target_->x_origin() + inset_;
    x_stop  = target_->x_origin() + target_->width() - inset_;
    y_start = y_stop = target_->y_origin() + target_->height() - 1;
    break;
  case SOUTH:
    x_start = target_->x_origin() + inset_;
    x_stop  = target_->x_origin() + target_->width() - inset_;
    y_start = y_stop = target_->y_origin();
    break;
  case EAST:
    x_start = x_stop = target_->x_origin();
    y_start = target_->y_origin() + target_->height() - inset_;
    y_stop  = target_->y_origin() + inset_;
    break;
  case WEST:
    x_start = x_stop = target_->x_origin() + target_->width() - 1;
    y_start = target_->y_origin() + target_->height() - inset_;
    y_stop  = target_->y_origin() + inset_;
    break;
  default:
    g_assert_not_reached();
  }

  target_->draw_line(x_start, y_start, x_stop, y_stop);

#ifdef G_ENABLE_DEBUG
  g_print("Drawing axis line from %d, %d to %d, %d\n", 
	  x_start, y_start, x_stop, y_stop);
#endif
}

void
Axis_Base::draw_tick(double pos, gint size, bool heavy)
{
  g_return_if_fail(target_ != 0);

  gint t = trans_.transform(pos);

  gint heaviness = heavy ? 2 : 1; // maybe configurable someday.
  
  while (heaviness > 0) 
    {
      if (align_ == NORTH) {
        target_->draw_line(t, target_->y_origin()+target_->height(),
                           t, target_->y_origin()+target_->height()-size);
      } else if (align_ == SOUTH) {
        target_->draw_line(t, target_->y_origin(),
                           t, target_->y_origin()+size);
      } else if (align_ == EAST) {
        target_->draw_line(target_->x_origin(), t, 
                           target_->x_origin()+size, t);
      } else { // if align == WEST
        target_->draw_line(target_->x_origin()+target_->width(), t,
                           target_->x_origin()+target_->width()-size, t);
      }
      
      --heaviness;
      ++t;
    }
}

void
Axis_Base::draw_label(double pos, gint adjust, const char* label,
                      bool superstyle, 
                      bool leftalign)
{
  g_return_if_fail(target_ != 0);

  gint t = trans_.transform(pos);

  // Turn off clipping while we draw some text, to avoid slicing off the
  // tops and bottoms of labels.  This is a hacky way to address this
  // problem, I know...
  target_->gc_clone();
  target_->gc()->set_noclip();

  if (superstyle)
    {
      // this is a pure hack FIXME
      target_->gc()->set_font("-*-*-bold-*-*-*-*-*-*-*-*-*-*-*");
    }

  if (leftalign) 
    {
      if (align_ == NORTH) {
        target_->draw_aligned_string(t, target_->y_origin()+target_->height(), label, 1, -1, 0, adjust);
      } else if (align_ == SOUTH) {
        target_->draw_aligned_string(t, target_->y_origin(), label, 1, 1, 0, adjust);
      } else if (align_ == EAST) {
        target_->draw_aligned_string(target_->x_origin(), t, label, 1, -1, adjust, 0);
      } else { // if align == WEST
        target_->draw_aligned_string(target_->x_origin()+target_->width(), t, label, -1, -1, adjust, 0);
      }
    }
  else 
    {
      if (align_ == NORTH) {
        target_->draw_aligned_string(t, target_->y_origin()+target_->height(), label, 0, -1, 0, adjust);
      } else if (align_ == SOUTH) {
        target_->draw_aligned_string(t, target_->y_origin(), label, 0, 1, 0, adjust);
      } else if (align_ == EAST) {
        target_->draw_aligned_string(target_->x_origin(), t, label, 1, 0, adjust, 0);
      } else { // if align == WEST
        target_->draw_aligned_string(target_->x_origin()+target_->width(), t, label, -1, 0, adjust, 0);
      }
    }


  target_->gc_restore();
}


void
Axis_Base::draw_numeric_label(double pos, gint adjust,
			      const char* format, double value)
{
  char buf[32];
  g_snprintf(buf, 32, format, value);
  draw_label(pos, adjust, buf);
}

