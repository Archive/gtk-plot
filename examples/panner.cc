// This is -*- C++ -*-
// $Id$

//
// Confidential Proprietary Material Exclusively Owned by EMC Capital
// Management.  (C) EMC Capital Management 1998.  All Rights Reserved.
//

#include <math.h>

#include <time.h>
#include <stdlib.h>
#include <gtk--.h>
#include <gtk--draw.h>
#include "plot_area.h"

#ifndef M_PI
#define M_PI 3.14159
#endif

class Gtk_Panner : public Gtk_DrawingBuffer, public Gtk_Idle {
public:
  Gtk_Panner() : Gtk_DrawingBuffer() {
    connect_to_method(button_press_event, this, &quit);

    N = 100000;
    xdata = new double[N];
    ydata = new double[N];
    
    for(size_t i=0; i<N; ++i) {
      xdata[i] = i/(double)N;
      double noise = 0.3*random()/(double)RAND_MAX-0.15;
      ydata[i] = sin(2*M_PI*xdata[i]) + noise;
    }
    xmin_ = 0; xmax_ = 0.1;
    dir_ = 1;
    sflag_ = true;
    time(&start_);
    last_ = start_;
    count_ = 0;
  } 

  gint quit(GdkEventButton* eb) {
    if (eb->button == 1) {
      sflag_ = !sflag_;
      cout << "Sortedness is " << (sflag_ ? "ON" : "OFF") << endl;
      time(&last_);
      count_ = 0;
    } else {
      Gtk_Main::instance()->quit();
    }
    return 0;
  }

  void redraw();
  void action() {
    
    xmin_ += dir_*0.02;
    xmax_ += dir_*0.02;
    if (xmax_ > 1.1 || xmin_ < -0.1)
      dir_ = -dir_;
    redraw();
    ++count_;

    time_t now;
    time(&now);
    if (now - last_ > 10) {
      cout << "frames/sec = " << count_ / (double)(now-last_) << endl;
      last_ = now;
      count_ = 0;
    }
  }

private:
  double* xdata;
  double* ydata;
  size_t N;
  double xmin_, xmax_;
  int dir_;
  bool sflag_;
  time_t start_, last_;
  int count_;
};

void
Gtk_Panner::redraw()
{
  stop();
  Gdk_Draw draw;
  draw.set_drawable(buffer());
  draw.gc()->set_foreground("white");
  draw.draw_rectangle(true, 0, 0, width(), height());
  draw.gc()->set_foreground("red");

  Plot_Area plot;
  plot.set_drawing_target(&draw);
  plot.set_x_bounds(xmin_, xmax_);
  plot.set_y_bounds(-1.5, 1.5);

  Plot_Data xpd(xdata, N);
  xpd.set_sorted(sflag_);
  Plot_Data ypd(ydata, N);

  plot.draw_scatter_plot(xpd, ypd);
  refresh();
  start();
}

main(int argc, char* argv[])
{
  Gtk_Main gtkmain(&argc, &argv);

  Gtk_Window main_window;
  Gtk_Panner thing;

  main_window.add( &thing );
  thing.show();
  main_window.show();


  gtkmain.run();

}



// $Id$
