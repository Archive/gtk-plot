// This is -*- C++ -*-
// $Id$

//
// Confidential Proprietary Material Exclusively Owned by EMC Capital
// Management.  (C) EMC Capital Management 1998.  All Rights Reserved.
//

#include <iostream.h>
#include "plot_data.h"

main()
{
  const double a[] = {1,2,3,4,5,6,7,8,9,10};
  const double b[] = {2,3,4,5,6,7,8,9,10,11};
  Plot_Data pda(a, 10);
  Plot_Data pdb(b, 10);
  pda.set_sorted(true);
  pdb.set_sorted(true);

  Plot_Data::const_iterator a0, a1, b0, b1;

  pda.find_bounding_iterators(0,6,a0,a1);
  b0 = pdb.sync(a0);
  b1 = pdb.sync(a1);
  
  if (a0 == a1)
    cout << "(* empty *)";
  while (a0 != a1) 
    cout << *(a0++) << "/" << *(b0++) << " ";
  cout << endl;
}




// $Id$
