// This is -*- C++ -*-
// $Id$

/*
 * dancing_line_graph.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include <math.h>

#include <gtk--draw.h>
#include "plot_area.h"

#ifndef M_PI
#define M_PI 3.14159
#endif

class Gtk_DancingLineGraph : public Gtk_DrawingBuffer, public Gtk_Timeout {
public:

  Gtk_DancingLineGraph() : Gtk_DrawingBuffer() { 

    connect_to_method(button_press_event, this, &quit);

    N = 300;
    xdata = new double[N];
    ydata = new double[N];

    phase = amplitude_theta = sigma_theta = 0;
    for(int i=0; i<N; ++i) 
      xdata[i] = 2*M_PI*i/N;
    data_update();

  }

  gint quit(GdkEventButton* eb) {
    Gtk_Main::instance()->quit();
    return 0;
  }

  void data_update() {
    for(int i=0; i<N; ++i)
      // This is my function.  The function that is mine.
      ydata[i] = fabs(cos(amplitude_theta))*sin((1+fabs(cos(sigma_theta)))*xdata[i] + phase) + cos(sin(sqrt(2)*sigma_theta)*xdata[i]+sqrt(3)*phase)/2 + sin((3*phase+xdata[i])/(1.01+sin(amplitude_theta/7)))/10;
  }

  void redraw();
  void action();

private:
  Gdk_Draw draw;
  Plot_Area plot;
  double* xdata;
  double* ydata;
  double phase, amplitude_theta, sigma_theta;
  int N;
};

void
Gtk_DancingLineGraph::redraw()
{
  stop();

  draw.set_drawable(screen(), buffer());
  draw.set_default_noclip();

  draw.gc()->set_foreground("blue");
  draw.draw_rectangle(true, 0, 0, width(), height());


  draw.set_default_clip_rectangle(20,20,width()-40,height()-40);
  draw.gc()->set_foreground("white");
  draw.draw_rectangle(true,0,0, width(), height());

  plot.set_drawing_target( &draw );
  plot.set_x_bounds(0,2*M_PI);
  plot.set_y_bounds(-1.2,1.2);

  draw.gc()->set_foreground("white");
  Plot_Data xpd(xdata, N), ypd(ydata, N);
  plot.reset_line_plot();
  draw.gc()->set_function(GDK_XOR);
  plot.draw_line_plot(xpd, ypd);
  draw.gc()->set_function(GDK_COPY);

  //  draw.set_default_noclip();

  start(20);
}

void
Gtk_DancingLineGraph::action()
{
  phase += M_PI/16;
  amplitude_theta += M_PI/37;
  sigma_theta += M_PI/99;
  data_update();

  Plot_Data xpd(xdata, N), ypd(ydata, N);
  
  draw.gc()->set_function(GDK_XOR);
  plot.redraw_line_plot();
  plot.draw_line_plot(xpd, ypd);
  draw.gc()->set_function(GDK_COPY);
}

main(int argc, char* argv[])
{
  Gtk_Main gtkmain(&argc, &argv);

  Gtk_Window main_window;
  Gtk_DancingLineGraph thing;

  main_window.add( &thing );
  thing.show();
  main_window.show();


  gtkmain.run();

}

// $Id$
