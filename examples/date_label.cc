// This is -*- C++ -*-
// $Id$

//
// Confidential Proprietary Material Exclusively Owned by EMC Capital
// Management.  (C) EMC Capital Management 1998.  All Rights Reserved.
//

#include "date_axis_labeller.h"
#include "numeric_axis_labeller.h"
#include "plot_area.h"

#include <math.h>
#include <stdlib.h>
#include <time.h>


gint quit() {
  Gtk_Main::instance()->quit();
  return 0;
}


static G_Date start_date;
static double step_size;
static guint step_count;

class C : public Gtk_Window {
public:
  C() 
    : box(true,0),
      start(start_date.julian(), 1, 3285000, 1, 100, 1),
      step(step_size, 1, 300000, 1, 10, 1),
      count(step_count, 1, 3000, 1, 15, 1),
      startb(start),
      stepb(step),
      countb(count)
    {
      connect_to_method(start.value_changed,
                        this,
                        &start_changed);

      connect_to_method(step.value_changed,
                        this,
                        &step_changed);

      connect_to_method(count.value_changed,
                        this,
                        &count_changed);

      add(&box);
      box.pack_start(startb,true,true,0);
      box.pack_start(stepb,true,true,0);
      box.pack_start(countb,true,true,0);

    }

  Gtk_VBox box;
  Gtk_Adjustment start;
  Gtk_Adjustment step;
  Gtk_Adjustment count;
  Gtk_SpinButton startb;
  Gtk_SpinButton stepb;
  Gtk_SpinButton countb;

  void start_changed()
    {
      start_date.set_julian(static_cast<guint>(startb.get_value_as_float()));
      redraw();
    }

  void step_changed()
    {
      step_size = stepb.get_value_as_float();
      redraw();
    }

  void count_changed()
    {
      step_count = countb.get_value_as_int();
      redraw();
    }

  Signal0 redraw;

  gint delete_event_impl(GdkEventAny* de) { quit(); return 1; }
};

class Gtk_TestPlot : public Gtk_DrawingBuffer {
public:
  void redraw();

};

int r() { return (random() % 51)-25; }

void
Gtk_TestPlot::redraw()
{
  // Make up some data
  const int N = step_count;
  double t[N], op[N], hi[N], lo[N], cl[N];
  double min=9450, max=9450;
  for(int i=0; i<N; ++i) {
    t[i] = (i != 0) ? t[i-1]+step_size : start_date.julian();
    //    if (i % 5 == 4) t[i] += step_size/5; // make the intervals semi-irregular
    cl[i] = (i ? cl[i-1] : 9450) + r();
    hi[i] = cl[i] + abs(r());
    lo[i] = cl[i] - abs(r());
    op[i] = cl[i] + r();
    if (op[i] > hi[i]) op[i] = hi[i];
    if (op[i] < lo[i]) op[i] = lo[i];

    if (hi[i] > max) max = hi[i];
    if (lo[i] < min) min = lo[i];
  }

  Axis_Base axW(Axis_Base::WEST);

  Numeric_Axis_Labeller labW(&axW);
  axW.set_bounds(min, max);

  labW.set_label_count_goal(20);

  Axis_Base axN(Axis_Base::NORTH);

  axN.set_bounds(t[0], t[N-1]);

  Date_Axis_Labeller labN(&axN);

  //  labN.set_label_count_goal(20);

  Gdk_Draw north_draw(screen(),buffer());
  Gdk_Draw west_draw(screen(),buffer());

  gint west_edge = 80;
  gint north_edge = 80;

  west_draw.set_default_clip_rectangle(0,north_edge,
				       west_edge,
				       height() - 2*north_edge);

  axW.set_drawing_target(&west_draw);

  north_draw.set_default_clip_rectangle(west_edge,0,
					width() - 2*west_edge,
					north_edge);

  axN.set_drawing_target(&north_draw);

  // Plots
  
  // Price bars
  Plot_Data td(t, N), od(op, N), hd(hi, N), ld(lo, N), cd(cl, N);

  Plot_Area plot1;
  Gdk_Draw plot_draw(screen(),buffer());
  plot_draw.set_default_clip_rectangle(west_edge,north_edge,
				       width() - 2 * west_edge,
				       height() - 2 * north_edge);
		
  plot1.set_drawing_target(&plot_draw);
  plot1.set_bounds(t[0],t[N-1],min,max);

  // marker tests.

  srandom(time(0));

  Plot_Area plot2;

  plot2.set_drawing_target(&plot_draw);
  plot2.set_bounds(0, 1000, 0, 1000);

  // Draw
  
  Gdk_Draw whole_screen(screen(),buffer());
  
  whole_screen.gc()->set_foreground("grey");
  whole_screen.draw_rectangle(true,0,0,width(),height());

  // white backdrop
  plot_draw.gc()->set_foreground("white");
  plot_draw.draw_rectangle(true,plot_draw.x_origin(),plot_draw.y_origin(),
			   plot_draw.width(),plot_draw.height());

  // Axes.
  west_draw.gc()->set_foreground("black");
  north_draw.gc()->set_foreground("black");

  axW.draw_axis_line();
  labW.label();
  axN.draw_axis_line();
  labN.label();

  plot_draw.gc()->set_foreground("red");
  plot1.draw_price_bars(td, od, hd, ld, cd);

#if 0
  Gdk_Color c;
  gint i = 0;
  gdouble divisor = .01;
  while ( i <= 20 ) {
    gdouble xdata[1000], ydata[1000];
    gdouble ycounter = 0.0;
    gdouble xcounter = 0.0;
    guint j = 0;
    while ( j < 1000 ) {
      xdata[j] = xcounter;
      xcounter += 20.0;
      ydata[j] = ycounter;
      ycounter += (xdata[j] + 500.0)/divisor;
      ++j;
      divisor += .03;
    }
    
    Plot_Data X(xdata, 1000), Y(ydata, 1000);

    c.set_random();
    plot_draw.gc()->set_foreground(c);
    if ( i < (gint)Plot_Area::VERTICAL_TICK ) {
      plot2.draw_scatter_plot(X, Y, (Plot_Area::marker_t)i);
    }
    else {
      plot2.draw_line_plot(X, Y);
    }
    
    ++i;
  }
#endif  

  refresh();
}

class MainWindow : public Gtk_Window {
public:
  gint delete_event_impl(GdkEventAny* de) { quit(); return 1; }
};


main(int argc, char* argv[])
{
  Gtk_Main gtkmain(&argc, &argv);

  start_date.set_dmy(1,G_DATE_JANUARY,1950);
  step_size = 365.0;
  step_count = 50;
    
  MainWindow main_window;
  Gtk_TestPlot tp;
  C c;

#if 0
  Gtk_ScrolledWindow sw;
  Gtk_Fixed box;
  box.add( &tp );
  sw.add_with_viewport( box );
  sw.set_policy(GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

  main_window.add( &sw );

#else 
  main_window.add(&tp);
#endif

  tp.set_usize(180, 180);
  main_window.set_usize(190, 190);

  main_window.set_position(GTK_WIN_POS_CENTER);
  main_window.set_policy(false, true, false);

  connect_to_method(c.redraw,
                    &tp,
                    &Gtk_TestPlot::redraw);

  c.show_all();

  main_window.show_all();
  c.set_usize(200,-1);

  gtk_window_set_transient_for(main_window.gtkobj(),c.gtkobj());

  gtkmain.run();
}

// $Id$
