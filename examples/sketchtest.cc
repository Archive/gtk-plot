// This is -*- C++ -*-
// $Id$

/*
 * sketchtest.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include <math.h>
#include <iostream.h>

#include <gtk--draw.h>
#include "plot_area.h"
#include "axis_base.h"
#include "numeric_axis_labeller.h"

class Gtk_SketchTest : public Gtk_DrawingBuffer {
public:
  Gtk_SketchTest() : Gtk_DrawingBuffer() {
    connect_to_method(button_press_event, this, &quit);
  }

  gint quit(GdkEventButton* eb) {
    Gtk_Main::instance()->quit();
    return 0;
  }

  void redraw();

};

double r(double s) { return s*((random() % 51)-25); }

void
Gtk_SketchTest::redraw()
{
  const edge = 40;

  // Make up some data
  const int N = 50;
  double t[N], op[N], hi[N], lo[N], cl[N];
  double initial= 9000+random()%9000;
  double scale=5*(1+(2.0*random()/RAND_MAX-1)/3);
  double min=initial, max=initial;
  for(int i=0; i<N; ++i) {
    t[i] = i ? t[i-1]+1 : 0;
    if (i % 5 == 4) t[i] += 2;
    cl[i] = (i ? cl[i-1] : initial) + r(scale);
    hi[i] = cl[i] + fabs(r(scale));
    lo[i] = cl[i] - fabs(r(scale));
    op[i] = cl[i] + r(scale);
    if (op[i] > hi[i]) op[i] = hi[i];
    if (op[i] < lo[i]) op[i] = lo[i];

    if (hi[i] > max) max = hi[i];
    if (lo[i] < min) min = lo[i];

  }

  Plot_Data td(t, N), od(op, N), hd(hi, N), ld(lo, N), cd(cl, N);

  Gdk_Draw draw(buffer());
  draw.gc()->set_foreground("grey");
  draw.draw_rectangle(true,0,0,width(),height());
  draw.set_default_clip_rectangle(edge, edge, width()-2*edge, height()-2*edge);
  draw.gc()->set_foreground("lightgrey");
  draw.draw_rectangle(true,0,0,width(),height());

  Axis_Base axW(Axis_Base::WEST);
  axW.set_bounds(min, max);
  Gdk_Draw axW_draw(buffer());
  axW_draw.set_default_clip_rectangle(0,edge,edge, height()-2*edge);
  axW.set_drawing_target(&axW_draw);
  axW_draw.gc()->set_foreground("black");
  axW.draw_axis_line();

  Axis_Base axE(Axis_Base::EAST);
  axE.set_bounds(&axW);
  Gdk_Draw axE_draw(buffer());
  axE_draw.set_default_clip_rectangle(width()-edge,edge,edge,height()-2*edge);
  axE.set_drawing_target(&axE_draw);
  axE_draw.gc()->set_foreground("black");
  axE.draw_axis_line();

  Numeric_Axis_Labeller nal(&axW);
  nal.set_label_count_goal(6);
  nal.set_locked_bounds(false);
  nal.set_mirror(&axE);
  nal.label();

  Plot_Area plot;

  plot.set_drawing_target(&draw);
  plot.set_x_bounds(-1,t[N-1]+1);
  plot.set_y_bounds(&axW);

  draw.gc()->set_foreground("red");
  plot.draw_price_bars(td, od, hd, ld, cd);

  refresh();
}

main(int argc, char* argv[])
{
  Gtk_Main gtkmain(&argc, &argv);
  Gtk_Window main_window;
  Gtk_SketchTest thing;

  main_window.add( &thing );
  thing.show();
  main_window.show();

  gtkmain.run();

}




// $Id$
