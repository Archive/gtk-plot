// This is -*- C++ -*-

/*
 * plotstuff.cc
 *
 * Copyright (C) 1998 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@emccta.com> and
 * Havoc Pennington <hp@emccta.com>.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifdef THIS_CODE_WORKS
#include <math.h>
#include <stdlib.h>
#include <time.h>

#include <gtk--draw.h>
#include <gtk--plot.h>

gint quit() {
  Gtk_Main::instance()->quit();
  return 0;
}

class Gtk_TestPlot : public Gtk_DrawingBuffer {
public:
  void redraw();

};

int r() { return (random() % 51)-25; }

void
Gtk_TestPlot::redraw()
{
  // Make up some data
  const int N = 50;
  double t[N], op[N], hi[N], lo[N], cl[N];
  double min=9450, max=9450;
  for(int i=0; i<N; ++i) {
    t[i] = i ? t[i-1]+1 : 0;
    if (i % 5 == 4) t[i] += 2;
    cl[i] = (i ? cl[i-1] : 9450) + r();
    hi[i] = cl[i] + abs(r());
    lo[i] = cl[i] - abs(r());
    op[i] = cl[i] + r();
    if (op[i] > hi[i]) op[i] = hi[i];
    if (op[i] < lo[i]) op[i] = lo[i];

    if (hi[i] > max) max = hi[i];
    if (lo[i] < min) min = lo[i];
  }

  Plot_Axis axW;
  axW.set_alignment(Plot_Axis::WEST);
  axW.set_axis_bounds(min, max);
  axW.set_axis_label("West Axis");
  axW.set_ticks((max-min)/40, "%.0f");

  Plot_Axis axN;

  axN.set_alignment(Plot_Axis::NORTH);
  axN.set_axis_bounds(0, t[N-1]);
  axN.set_axis_label("North Axis");
  axN.set_ticks(t[N-1]/30, "%.0f");

  Gdk_Draw north_draw(screen(),buffer());
  Gdk_Draw west_draw(screen(),buffer());

  gint west_edge = axW.get_suggested_size(west_draw.gc());
  gint north_edge = axN.get_suggested_size(north_draw.gc());

  west_draw.set_default_clip_rectangle(0,north_edge,
				       west_edge,
				       height() - 2*north_edge);

  axW.set_drawing_target(&west_draw);

  north_draw.set_default_clip_rectangle(west_edge,0,
					width() - 2*west_edge,
					north_edge);
  axN.set_drawing_target(&north_draw);

  axW.set_default_transform();
  axN.set_default_transform();

  axW.set_mirror_offset(width()-west_edge);
  axN.set_mirror_offset(height()-north_edge);

  // Plots
  
  // Price bars
  Plot_Data td(t, N), od(op, N), hd(hi, N), ld(lo, N), cd(cl, N);

  Plot_Area plot1;
  Gdk_Draw plot_draw(screen(),buffer());
  plot_draw.set_default_clip_rectangle(west_edge,north_edge,
				       width() - 2 * west_edge,
				       height() - 2 * north_edge);
		
  plot1.set_drawing_target(&plot_draw);
  plot1.set_plot_bounds(0,t[N-1],min,max);
  plot1.set_x_transform(*axN.transform());
  plot1.set_y_transform(*axW.transform());

  // marker tests.

  srandom(time(0));

  Plot_Area plot2;

  plot2.set_drawing_target(&plot_draw);
  plot2.set_plot_bounds(0, 1000, 0, 1000);
  plot2.set_default_transforms(); // axes don't correspond to this plot.

  // Draw
  
  Gdk_Draw whole_screen(screen(),buffer());
  
  whole_screen.gc()->set_foreground("grey");
  whole_screen.draw_rectangle(true,0,0,width(),height());

  // white backdrop
  plot_draw.gc()->set_foreground("white");
  plot_draw.draw_rectangle(true,plot_draw.x_origin(),plot_draw.y_origin(),
			   plot_draw.width(),plot_draw.height());

  // Axes.
  west_draw.gc()->set_foreground("black");
  north_draw.gc()->set_foreground("black");

  axW.draw_complete_axis();

  axN.draw_complete_axis();

  plot_draw.gc()->set_foreground("red");
  plot1.draw_price_bars(td, od, hd, ld, cd);

  Gdk_Color c;
  gint i = 0;
  gdouble divisor = .01;
  while ( i <= 20 ) {
    gdouble xdata[1000], ydata[1000];
    gdouble ycounter = 0.0;
    gdouble xcounter = 0.0;
    guint j = 0;
    while ( j < 1000 ) {
      xdata[j] = xcounter;
      xcounter += 20.0;
      ydata[j] = ycounter;
      ycounter += (xdata[j] + 500.0)/divisor;
      ++j;
      divisor += .03;
    }
    
    Plot_Data X(xdata, 1000), Y(ydata, 1000);

    c.set_random();
    plot_draw.gc()->set_foreground(c);
    if ( i < (gint)Plot_Area::VERTICAL_TICK ) {
      plot2.draw_scatter_plot(X, Y, (Plot_Area::marker_t)i);
    }
    else {
      plot2.draw_line_plot(X, Y);
    }
    
    ++i;
  }
  
  refresh();
}

class MainWindow : public Gtk_Window {
public:
  gint delete_event_impl(GdkEventAny* de) { quit(); return 1; }
};
#endif

main(int argc, char* argv[])
{
#ifdef THIS_CODE_WORKS
  Gtk_Main gtkmain(&argc, &argv);
  
  
  MainWindow main_window;
  Gtk_ScrolledWindow sw;
  Gtk_Fixed box;
  Gtk_TestPlot tp;

  box.add( &tp );
  sw.add( &box );
  sw.set_policy(GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  box.set_focus_vadjustment(sw.get_vadjustment());
  box.set_focus_hadjustment(sw.get_hadjustment());
  main_window.add( &sw );
  main_window.set_usize(700, 700);
  main_window.position(GTK_WIN_POS_CENTER);
  tp.set_usize(700, 700);

  main_window.show_all();

  gtkmain.run();
#endif
}

